# **ARM**

- Dominante en el mercado movil
  - Baja Potenica y simple
- Arquitectura Load/Store
  - Las instrucciones no se ejecutan contra la memoria
  - Instrucciones entre registros
- Instucciones de longitud fija -> **32 bits**
- Formato de intrucciones de 3 direcciones
  - 2 registros operandos
  - 1 registro de resultado
- Ejecucion condicional de **todas** las instrucciones
- Instrucciones de Load-Store de registros multiles

### Organizacion de Registros

- R0 a R12 son registros de propósito general (32 bits)
  - Usados por el programador para (casi) cualquier propósito sin restricción
- R13 es el Stack Pointer (SP)
- R14 es el Link Register (LR)
- R15 es el Program Counter (PC)
- El Current Program Status Register (CPSR) contiene indicadores condicionales y otros bits de estado

![ARM Current Program Status Register](Images/ARM-CPSR.png)

### Organización de Memoria

- Máximo: 232 bytes de memoria
- Word = 32-bits@ Half-word = 16 bits
- Words están alineadas en posiciones divisibles por 4
- Half-words están alineadas en posiciones pares
Estructura de un Programa
- La forma general de una línea en un módulo ARM es: label <espacio> opcode <espacio> operandos
<espacio> @ comentario
- Cada campo debe estar separado por uno o más espacios.
- Las instrucciones no empiezan en la primer columna, dado que deben estar precedidas por un espacio
en blanco, incluso aunque no haya label.
- ARM aceptará líneas en blanco para mejorar la claridad del código
DOBLAR

## Interrupciones de Software
- Una forma de leer información y mostrar resultados
- Instrucción swi que provoca una interrupción de software
- SWI Codes :
  - Imprimir un entero :
    - Operando 0x6B
    - Registro r1 contiene el entero a imprimir
    - El registro r0 contiene dónde imprimirlo
  - Leer entero desde un Archivo
    - Operando 0x6c
    - R0: manejador de archivo
    - R0: entero
  - Obtener el tiempo actual (ticks)
    - Operando 0x6d
    - R0: número de ticks (milisegundos)
  - Leer string desde un Archivo
    - Operando 0x6a
    - R0: manejador de archivo R1: dirección destino R2: max bytes a almacenar
    - R0: número de bytes almacenados
  - Escribir string
    - Operando 0x69
    - R0: manejador de archivo o Stdout (1) R1: dirección de un string terminado en null
  - Cerrar Archivo
    - Operando 0x68
    - R0: manejador de archivo
  - Abrir Archivo (Modo 0: Input / Modo 1: Output / Modo 2: Append)
    - Operando 0x66
    - R0: dirección de un string terminado en null con el nombre del archivo R1: Modo
    - R0: manejador de archivo (-1 si el archivo no abre)
  - Desasignar todo Bloque de Memoria
    - Operando 0x13
  - Asignar Bloque de Memoria
    - Operando 0x12
    - R0: tamaño del Bloque en bytes
    - R0: dirección del Bloque
  - Mostrar string por consola
    - Operando 0x02
    - R0: dirección de un string terminado en null
  - Mostrar caracter por consola
    - Operando 0x00
    - R0: el caracter
  - fin del programa/Detener la ejecución :
    - Operando 0x11

## Secciones del programa

- .text especifica la sección de código
- .data especifica la sección de variables

## Transferencia de datos de un registro

- ldr @ Load Word
- str @ Store Word
- ldrb @ Load Byte
- strb @ Store Byte
- ldrh @ Load Halfword
- strh @ Store Halfword
- ldrsb @ Load Signed Byte (load and extent sign to 32 bits) ldrsh @ Load Signed Halfword (load and extent sign to 32 bits)

## Instrucciones Aritméticas

- Add
  - ADD{cond}{S} Rd, Rn, <Oprnd2>
- Subtract ——> Agarra el segundo operando y le resta el tercero y lo guarda en el primero
  - SUB{cond}{S} Rd, Rn, <Oprnd2>
- Reverse subtract ——> Agarra el tercer operando y le resta el segundo y lo guarda en el primero (al revés de la resta común )
  - RSB{cond}{S} Rd, Rn, <Oprnd2>
- Multiply
  - MUL{cond}{S} Rd, Rm, Rs

## Instrucciones Lógicas

- And
  - AND{cond}{S} Rd, Rn, <Oprnd2>
- Exclusive Or
  - EOR{cond}{S} Rd, Rn, <Oprnd2>
- Or
|- ORR{cond}{S} Rd, Rn, <Oprnd2>
