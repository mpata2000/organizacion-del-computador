Nota: Entre parentesis se consignan los digitos periodicos.


SN1.- Pasar los siguientes numeros de la base indicada a base 10.

    a. 19[16]

        1 0
        1 9[16] = 9.16^(0)+1.16 [10]=25[10]
        .-----------------.
        | 19[16] = 25[10] |
        '-----------------' 

    b. 0,68[9]

        0 -1 -2
        0, 6  8 = 6.9^(-1)+8.9^(-2) [10]=0,765[10]
        .---------------------.
        | 0,68[9] = 0,765[10] |
        '---------------------' 

    c. 3,(57)[8] 

        3,(57)[8] = 3 + 57/77 [8]

        > 3[8]  = 3[10]
        > 57[8] = 7 + 5.8 [10] = 47[10]
        > 77[8] = 7 + 7.8 [10] = 63[10]

        .-------------------------------------.
        | 3,(57)[8] = 3+47/63[10] = 3,746[10] |
        '-------------------------------------' 

    d. 15[15]

        1 0
        1 5[15] = 5 + 1.15 [10] = 20[10]
        .-----------------.
        | 15[15] = 20[10] |
        '-----------------' 

    e. 0,CAFE[16]

        0  -1 -2 -3 -4
        0 , C  A  F  E [16] = 12.16^(-1)+10.16^(-2)+15.16^(-3)+14.16^(-4) [10]= 0,7929[10]
        .-------------------------.
        |  0,CAFE[16]= 0,7929[10] |
        '-------------------------' 

    f. 7,B[16]

        7,B[16] = 7 + 11.16^(-1) [10] = 7,6875[10]
        .----------------------.
        | 7,B[16] = 7,6875[10] |
        '----------------------' 

    g. 0,2(13)[4]

        0,2(13)[4] = (213-2)/330 [4]

        > 213[4] = 3 + 1.4 + 2.8 [10] = 39[10]
        > 2  [4] = 2[10]
        > 330[4] = 3.4 + 3.8 [10] = 60[10] 
        .------------------------------------------.
        | 0,2(13)[4] = (39-2)?60[10] = 0,61(6)[10] |
        '------------------------------------------' 

    h. 0,111[2] 

        0,111[2] = 2^(-1)+2^(-2)+2^(-3) [10] = 0,875[10]
        .----------------------.
        | 0,111[2] = 0,875[10] |
        '----------------------' 

    i. 654[8] 

        654[8] = 4 + 5.8 + 6.8^2 [10] = 428[10]
        .------------------.
        | 654[8] = 428[10] |
        '------------------'

    j. 0,(2)[3] 

        0,(2)[3] = 2/2 [3] = 2/2[10] = 1[10]
        .------------------.
        | 0,(2)[3] = 1[10] |
        '------------------'

    k. 53[14] 

        53[14] = 3 + 5.14 [10] = 73[10]
        .-----------------.
        | 53[14] = 73[10] |
        '-----------------'

    l. 123[8]

        123[8] = 3 + 2.8 + 1.8^2 [10] = 83[10]
        .-----------------.
        | 123[8] = 83[10] |
        '-----------------'



SN2.- Pasar los siguientes numeros de base 10 a la base indicada.
    
    a. 19[10] --> [6]

        19/6 = 3 resto 1
                       3 <- resto de 3/6
        .----------------.
        | 19[10] = 31[6] |
        '----------------'

    b. 5,35[10] --> [9]

        5[10]=5[9]

        0,35 . 9 = 3,15  | 3
        0,15 . 9 = 1,35  | 1  -> Ciclo

        .----------------------.
        | 5,35[10] = 5,(31)[6] |
        '----------------------'
    
    c. 6,(21)[10] --> [8]

        6,(21)[10]= 6 + 21/99 [10] = 205/33 [10]

            > 205[10]->[8]
              205/8 = 25 resto 5
              25/8  =  3 resto 1
                               3
              205[10] = 315[8]

            > 33[10]->[8]
              33/8 = 4 resto 1
                             4
              33[10] = 41[8]               
    
        .-------------------------.
        | 6,(21)[10] = 315/41 [8] |
        '-------------------------'
    
    d. 5[10] --> [15]

        .---------------.
        | 5[10] = 5[15] |
        '---------------'
    
    e. 0,56[10] --> [16]

        0,56 . 16 =  8,96  | 8
        0,96 . 16 = 15,36  | F
        0,36 . 16 =  5,76  | 5
        0,76 . 16 = 12,16  | C
        0,16 . 16 =  2,56  | 2 -> Ciclo

        .--------------------------.
        | 0,56[10] = 0,(8F5C2)[16] |
        '--------------------------'
    
    f. 7,48[10] --> [16]

        > 7[10] = 7[16]

        > 0,48[10]->[16]
          0,48 . 16 =  7,68  | 7
          0,68 . 16 = 10,88  | A
          0,88 . 16 = 14,08  | E
          0,08 . 16 =  1,28  | 1
          0,28 . 16 =  4,48  | 4 -> Ciclo
    
    g. 0,(3)[10] --> [3]

        0,(3)[10] = 3/9 [10] = 1/3[10]

        .---------------------.
        | 0,(3)[10] = 1/10[3] |
        '---------------------'
    
    h. 45[10] --> [8]

        45/8 = 5 resto 5
                       5
        .----------------.
        | 45[10] = 55[8] |
        '----------------'               
    
    i. 13[10] --> [14]

        .----------------.
        | 13[10] = D[14] |
        '----------------'
    
    j. 9,15(2)[10] --> [5]

        9,15(2)[10] = 9,15 + 0,00(2) = 9,15 + 1/450 [10]  <- resuelvo asi para quede igual que en la guia de resueltos

        > 9[10]->[5]
          9/5 = 1 resto 4
                        1
          9[10] = 14[5]   
        
        > 0,15[10]->[5]
          0,15 . 5 = 0,75  | 0
          0,75 . 5 = 3,75  | 3 -> periodico

          0,15[10] = 0,0(3)    

        > 450[10]->[5]
          450/5 = 90 resto 0
           90/5 = 18 resto 0
           18/5 =  3 resto 3
                           3
          450[10] = 3300[5]                        

        .------------------------------------.
        | 9,15(2)[10] = 14,0(3) + 1/3300 [5] |
        '------------------------------------'    


SN3.- Pasar los siguientes números a las bases indicadas.


    a. 45[8]-->[7]
        
        > 45[8]->[10]
          45[8] = 5 + 4.8 [10] = 37[10]

        > 37[10]->[7]
          37/7=5 resto 2
                       5
          37[10] = 52[7]

        .---------------.
        | 45[8] = 52[7] |
        '---------------' 

    b. 1010,01[2]-->[9]

        > 1010,01[2]->[10]
          3 2 1 0  -1 -2
          1 0 1 0 , 0  1 [2] = 2^(-2) + 2 + 2^3 = 10.25 [10]

        > 10.25[10]->[9]
          Parte entera:
            10/9 = 1 resto 1
                           1
          Parte decimal:
            0,25.9 = 2,25  | 2 periodico  
          
          10.25[10] = 11,(2)[9] 

        .-----------------------.
        | 1010,01[2]= 11,(2)[9] |
        '-----------------------'   

    c. 0,(1A)[16]-->[8]

        0,(1A)[16] = 1A/FF [16]
        
        Numerador:
          > 1A[16]->[10]
            1A[16] = 10 + 16 [10] = 26[10]
  
          > 26[10]->[8]
            26/8 = 3 resto 2
                           3
            26[10] = 32[8]
        
        Denominador:
          > FF[16]->[10]
            FF[16] = 15 + 15.16 = 255[10]

          > 255[10]->[8]
            255/8 = 31 resto 7
             31/8 =  3 resto 7
                             3
            255[10] = 377[8]

        .-------------------------.
        | 10,(1A)[16]= 32/377 [8] |
        '-------------------------' 

    d. 54321[6]-->[8]

        > 54321[6]->[10]
         4 3 2 1 0
         5 4 3 2 1[6] = 1 + 2.6 + 3.6^2 + 4.6^3 + 5.6^4 = 7465[10]
         54321[6] = 7465[10]
        
        > 7465[10]->[8]
          7465/8 = 933 resto 1
           933/8 = 116 resto 5
           116/8 =  14 resto 4
            14/8 =   1 resto 6
                             1
          7465[10] = 16451[8]

        .---------------------.
        | 54321[6] = 16451[8] |
        '---------------------' 

    e. 7,B[16]--> [11]

        > 7,B[16]->[10]
          7,B[16] = 7 + 11.16^(-1) = 7,6875[10]

        > 7,6875[10] -> [11]
          
          Parte entera: 7[10] = 7[11]

          Parte decimal  
            0,6875 . 11 = 7,5625  | 7
            0,5625 . 11 = 6,1875  | 6
            0,1875 . 11 = 2,0625  | 2
            0,0625 . 11 = 0,6875  | 0 -> Ciclo

          7,6875[10] = 7,(7620)[11]
        
        .------------------------.
        | 7,B[16] = 7,(7620)[11] |
        '------------------------' 

    f. 0,2(3)[7] --> [9] 

        0,2(3)[7] = (23-2)/66 [7] = 21/66[7]

        > 21/66[7]->[10]

          Numerador:
            21[7] = 1 + 2.7 [10] = 15[10]

          Denominador:
            66[7] = 6 + 6.7 [10] = 48[10]  

          21/66[7]= 15/48[10] = 5/16[10]

        > 5/16[10] -> 9
          Numerador:
            5[10] = 5[9]
          Denominador:
            16/9 = 1 resto 7
                           1
            16[10] = 17[9]

        .----------------------.
        | 0,2(3)[7] = 5/17 [9] |
        '----------------------'               

    g. 0,111[2]-->[5] 

        > 0,111[2]->[10]
          0,111[2] = 2^(-1) + 2^(-2) + 2^(-3) [10] = 0,875[10]

        > 0,875[10]->[5]
          0,875 . 5 = 4.375  | 4
          0,375 . 5 = 1.875  | 1 -> Ciclo
          0,875[10] = 0,(41)[5]

        .----------------------.
        | 0,111[2] = 0,(41)[5] |
        '----------------------'  

    h. 56[8]-->[3]

        > 56[8]->[10]
          56[8] = 6 + 5.8 [10] = 46[10]
          
        > 46[10]->[3]
          46/3 = 15 resto 1
          15/3 =  5 resto 0
           5/3 =  1 resto 2
                          1
          46[10] = 1201[3] 

        .-----------------.
        | 56[8] = 1201[3] |
        '-----------------'               

    i. 3,(2)[4]-->[2]

        Usando regla de la raiz

        3[4] = 11[2]
        2[4] = 10[2]

        .-----------------------.
        | 3,(2)[4] = 11,(10)[2] |
        '-----------------------'  

    j. ABC[16]-->[5]

        > ABC[16]->[10]
          ABC[16] = 12 + 11.16 + 10.16^2 [10] = 2748[10]

        > 2748[10]->[5]
          2748/5 = 549 resto 3
           549/5 = 109 resto 4
           109/5 =  21 resto 4
            21/5 =   4 resto 1
                             4
          2748[10] = 41443[5]

        .--------------------.
        | ABC[16] = 41443[5] |
        '--------------------'  

SN4.- Pasar los siguientes números a las bases indicadas usando propiedad
de potencia o raíz de base.

    a. 75[4]-->[16]

        75 no es un numero valido en base 4 entoces no se puede resolver el ejercicio

    b. BCD,EF[16]-->[2]

        La base 16 es potencia exacta (4) de 2
        
                    a=b^x --> 16=2^4

        Expando un digito de la base 16 y genero 4 digitos de la base 2

            B  |   C  |   D  |,|   E  |   F       <- Hexa
           11  |  12  |  13  |,|  14  |  15       <- Decimal
          1011 | 1100 | 1101 |,| 1110 | 1111      <- Binario

        .---------------------------------------.
        | BCD,EF[16] = 101111001101,11101111[2] |
        '---------------------------------------'

    c. 2211[3]-->[9]

        Si la base a es raiz exacta (x) de b

            a^x=b --> 3^2=9

        Agrupamos de a 2 digitos de la base 3 y genero un digito de la base 9

         22 | 11
          8 |  4

        .-----------------.
        | 2211[3] = 84[9] |
        '-----------------' 

    d.1000,01[2]-->[8]

        Si la base a es raiz exacta (x) de b

            a^x=b --> 2^3=8

        Agrupamos de a 3 digitos de la base 2 y genero un digito de la base 8

         001 | 000 |,| 010   -> C0mpleto con 0 a la izquierda y la derecha para que haya 3 digitos de base 2 por cada numero de base 8
          1  |  0  |,|  2 

        .----------------------.
        | 1000,01[2] = 10,2[8] |
        '----------------------' 


    e.1001[2]-->[16]
    
        Si la base a es raiz exacta (x) de b

            a^x=b --> 2^4=16

        Agrupamos de a 4 digitos de la base 2 y genero un digito de la base 16

        .-----------------.
        | 1001[2] = 9[16] |
        '-----------------' 

SN5.- Realizar las siguientes sumas en la base indicada:

    a. 101 + 1010 [2]

           101
          1010 +
        -------
          1111

        .--------------------------.
        | 101 + 1010 [2] = 1111[2] |
        '--------------------------' 

    b. 5423 + 2134 [8]

          5423
          2134 +
        -------
          7557

        .---------------------------.
        | 5423 + 2134 [8] = 7557[8] |
        '---------------------------' 

    c. CDDE + 1F1F [16]

        [16] -> 0 - 1 - 2 - 3 - 4 - 5 - 6 - 7 - 8 - 9 - A - B - C - D - E - F

        0 1 0 1       <- Carry
          C D D E
          1 F 1 F +
        -----------
          E C F D

        .------------------------------.
        | CDDE + 1F1F [16] = ECFD [16] |
        '------------------------------'

    d. 1011 + 1111 [2]

          1 1 1      <- Carry
          1 0 1 1
          1 1 1 1 +
        -----------
        1 1 0 1 0 

        .----------------------------.
        | 1011 + 1111 [2] = 11010[2] |
        '----------------------------' 
    
    e. 1213 + 2113 [4]

          0 0 1      <- Carry
          1 2 1 3
          2 1 1 3 +
        -----------
          3 3 3 2 

        .---------------------------.
        | 1213 + 2113 [4] = 3332[4] |
        '---------------------------' 

    f. 7231 + 3025 [8]

        1 0 0 0      <- Carry
          7 2 3 1
          3 0 2 5 +
        -----------
        1 2 2 5 6 

        .----------------------------.
        | 7231 + 3025 [8] = 12256[8] |
        '----------------------------'     

SN6.- Realizar las siguientes restas en la base indicada:

    a. 1101 - 10 [2]

               10      <- Carry
          1  0 10  1    <- Numero que queda
          1  1  0  1
                1  0 -
        --------------
          1  0  1  1

        .-------------------------.
        | 1101 - 10 [2] = 1011[2] |
        '-------------------------' 

    b. 5423 - 1111 [8]

          5 4 2 3
          1 1 1 1 +
        -----------
          4 3 1 2 

        .---------------------------.
        | 5423 - 1111 [8] = 4312[8] |
        '---------------------------'  

    c. DDE - F1F [16]

        [16] -> 0 - 1 - 2 - 3 - 4 - 5 - 6 - 7 - 8 - 9 - A - B - C - D - E - F
        
        DDE-F1F = (-1) . (F1F - DDE)

             10        <- Carry
          E  11   F    <- Numero que queda
          F   1   F 
          D   D   E -
        ------------
         1   4   1

         -141

        .---------------------------.
        | DDE - F1F [16] = -141[16] |
        '---------------------------' 

    d. 1011 - 1111 [2]

        1011-1111 = (-1) . (1111 - 1011)

          1 1 1 1
          1 0 1 1 -
        -----------
          0 1 0 0

          -100

        .---------------------------.
        | 1011 - 1111 [2] = -100[2] |
        '---------------------------' 

    e. A213 - 2F1B [16]

        [16] -> 0 - 1 - 2 - 3 - 4 - 5 - 6 - 7 - 8 - 9 - A - B - C - D - E - F
        
             10  10  10   <- Carry
          9  11  10  13  <- Numero que queda
          A   2   1   3 
          2   F   1   B -
        ---------------
          7   2   F   8

        .-----------------------------.
        | A213 - 2F1B [16] = 72F8[16] |
        '-----------------------------' 

    f. 1111 - 1011 [2]

          1 1 1 1
          1 0 1 1 -
        -----------
          0 1 0 0

        .--------------------------.
        | 1011 - 1111 [2] = 100[2] |
        '--------------------------' 


SN7.- Dados los siguientes números, almacenarlos en formato binario de punto fijo sin signo de 8 y 16 bits e indicar su configuración decimal y hexadecimal.

    a. 23 [10]

        > Paso a base 2
          23/2 = 11 resto 1
          11/2 =  5 resto 1
           5/2 =  2 resto 1
           2/2 =  1 resto 0
                          1

          23[10] = 10111[2]

        > Completo con 0s 

          0001 0111 BPF s/s 8 bits

          0000 0000 0001 0111 BPF s/s 16 bits
        
        > Transformo a la configuraciones

          10111[2] = 17[16]
          10111[2] = 23[10]

        .-------------------------------------.
        | 0001 0111 BPF s/s 8 bits            |
        |     > Conf. Dec: 23[10]             |
        |     > Conf. Dec: 17[16]             |
        |                                     |
        | 0000 0000 0001 0111 BPF s/s 16 bits |
        |     > Conf. Dec: 23[10]             |
        |     > Conf. Dec: 17[16]             |
        '-------------------------------------' 



    b. 8320 [9]

        8320 [9] = 2.9 + 3.9^2 + 8.9^3 = 6093[10]

        > Paso a base 2

          6093/2 = 3046 resto 1     
          3046/2 = 1523 resto 0
          1523/2 =  761 resto 1
           761/2 =  380 resto 1
           380/2 =  190 resto 0
           190/2 =   95 resto 0
            95/2 =   47 resto 1
            47/2 =   23 resto 1
            23/2 =   11 resto 1
            11/2 =    5 resto 1
             5/2 =    2 resto 1
             2/2 =    1 resto 0
                              1

          6093[10] = 1011111001101[2]

        > Completo

          No entra en 8 bits

          0001 0111 1100 1101 BPF s/s 16 bits

        > Paso a las configuraciones

         0001 0111 1100 1101[2] = 17CD[16]
         0001 0111 1100 1101[2] = 6093[10]

        .-------------------------------------.
        | NO entra en 8 bits                  |
        |     > Conf. Dec: -                  |
        |     > Conf. Dec: -                  |
        |                                     |
        | 0001 0111 1100 1101 BPF s/s 16 bits |
        |     > Conf. Dec: 6093[10]           |
        |     > Conf. Dec: 17CD[16]           |
        '-------------------------------------'  

    c. 72114 [11]

        72114 [11] = 4 + 11 + 11^2 + 2.11^3 + 7.11^4 = 105285[10]

        > Paso a base 2

          105285/2 = 52642 resto 1
           52642/2 = 26321 resto 0
           26321/2 = 13160 resto 1
           13160/2 =  6580 resto 0
            6580/2 =  3290 resto 0
            3290/2 =  1645 resto 0
            1645/2 =   822 resto 1
             822/2 =   411 resto 0
             411/2 =   205 resto 1
             205/2 =   102 resto 1
             102/2 =    51 resto 0
              51/2 =    25 resto 1
              25/2 =    12 resto 1
              12/2 =     6 resto 0
               6/2 =     3 resto 0
               3/2 =     1 resto 1
                                 1

        105285[10] = 1 1001 1011 0100 0101[2] son 17 bit no estra en ninguno de los 2

        .-------------------------------------.
        | NO entra en 8 bits                  |
        |     > Conf. Dec: -                  |
        |     > Conf. Dec: -                  |
        |                                     |
        | NO entra en 16 bits                 |
        |     > Conf. Dec: -                  |
        |     > Conf. Dec: -                  |
        |                                     |
        .-------------------------------------.
                   


SN8.- Dadas las siguientes configuraciones de binarios de punto fijo sin signo
de 32 bits, indicar el número que se encuentra almacenado en base 10.

    a. C66EA940 [16]

        > Paso a base [2]

            |   C  |   6  |  6   |   E  |   A  |   9  |   4  |   0  |
            | 1100 | 0110 | 0110 | 1110 | 1010 | 1001 | 0100 | 0000 | 

            C66EA940 [16] = 1100 0110 0110 1110 1010 1001 0100 0000[2]

            1100 0110 0110 1110 1010 1001 0100 0000 BPF s/s 32 bits

        > Paso a base [10]
            31 30 29 28 27 26 25 24 23 22 21 20 19 18 17 16 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
             1  1  0  0  0  1  1  0  0  1  1  0  1  1  1  0  1  0  1  0  1  0 0 1 0 1 0 0 0 0 0 0 [2] = 
             
               = 2^31 + 2^30 + 2^26 + 2^25 + 2^22 + 2^21 + 2^19 + 2^18 + 2^17 + 2^15 + 2^13 + 2^11 + 2^8 + 2^6 [10] = 3.329.141.056 [10]


        .------------------------------------------------------------------.
        | Configuracion: C66EA940 [16]                                     |
        |                                                                  |
        | Formato: 1100 0110 0110 1110 1010 1001 0100 0000 BPF s/s 32 bits |
        |                                                                  |
        | Valor: 3.329.141.056 [10]                                        |
        '------------------------------------------------------------------' 

    b. 276012 [8]

        > Paso a base [2]

          |  2  |  7  |  6  |  0  |  1  |  2  |
          | 010 | 111 | 110 | 000 | 001 | 010 |

           276012 [8] = 010 111 110 000 001 010[2]

        > Expando a BPF s/s 32 bits 

            010 111 110 000 001 010[2]
            
            0000 0000 0000 0001 0111 1100 0000 1010 BPF s/s 32 bits

        > Paso a base [10]

          Ignoro los  a la izquierda
            16 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0 
             1  0  1  1  1  1  1 0 0 0 0 0 0 1 0 1 0 [2] = 2 + 2^3 + 2^10 + 2^11 + 2^12 + 2^13 + 2^14 + 2^16 [10] = 97.290 [10]

        .------------------------------------------------------------------.
        | Configuracion: 276012 [8]                                        |
        |                                                                  |
        | Formato: 0000 0000 0000 0001 0111 1100 0000 1010 BPF s/s 32 bits |
        |                                                                  |
        | Valor: 97.290 [10]                                               |
        '------------------------------------------------------------------' 

    c. F64C6F5B [16]
    
        > Paso a base[2]

            |   F  |   6  |  4   |   C  |   6  |   F  |   5  |   B  |
            | 1111 | 0110 | 0100 | 1100 | 0110 | 1111 | 0101 | 1011 | 


            1111 0110 0100 1100 0110 1111 0101 1011 BPF s/s 32 bits

        > Paso a base [10]

             31 30 29 28 27 26 25 24 23 22 21 20 19 18 17 16 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
              1  1  1  1  0  1  1  0  0  1  0  0  1  1  0  0  0  1  1  0  1  1 1 1 0 1 0 1 1 0 1 1  [2] = 
                            
                = 1 + 2 + 8 + 16 + 2^6 + 2^8 + 2^9 + 2^10 + 2^11 + 2^13 + 2^14 + 2^18 + 2^19 + 2^22 + 2^25 + 2^26 + 2^28 + 2^29 + 2^30 + 2^31 [10] = 4.132.204.379 [10]
    
        .------------------------------------------------------------------.
        | Configuracion: F64C6F5B [16]                                     |
        |                                                                  |
        | Formato: 1111 0110 0100 1100 0110 1111 0101 1011 BPF s/s 32 bits |
        |                                                                  |
        | Valor: 4.132.204.379 [10]                                        |
        '------------------------------------------------------------------' 


SN9.- Idem ejercicio 7 para binario de punto fijo con signo.

    AL ser todos los numeros positivos da los mismos pasos y resultados

    a. 23[10]
        .-------------------------------------.
        | 0001 0111 BPF s/s 8 bits            |
        |     > Conf. Dec: 23[10]             |
        |     > Conf. Dec: 17[16]             |
        |                                     |
        | 0000 0000 0001 0111 BPF s/s 16 bits |
        |     > Conf. Dec: 23[10]             |
        |     > Conf. Dec: 17[16]             |
        '-------------------------------------' 

    b. 8320 [9]

        .-------------------------------------.
        | NO entra en 8 bits                  |
        |     > Conf. Dec: -                  |
        |     > Conf. Dec: -                  |
        |                                     |
        | 0001 0111 1100 1101 BPF s/s 16 bits |
        |     > Conf. Dec: 6093[10]           |
        |     > Conf. Dec: 17CD[16]           |
        '-------------------------------------'  

    c. 72114 [11]

        .-------------------------------------.
        | NO entra en 8 bits                  |
        |     > Conf. Dec: -                  |
        |     > Conf. Dec: -                  |
        |                                     |
        | NO entra en 16 bits                 |
        |     > Conf. Dec: -                  |
        |     > Conf. Dec: -                  |
        |                                     |
        .-------------------------------------.

SN10.- Idem ejercicio 8 para binario de punto fijo con signo.

    a. C66EA940[16]

        Del SN8-a sabemos que el formato es 1100 0110 0110 1110 1010 1001 0100 0000 pero esta vez BPF c/s 32 bits

        El numero arranca con 1, entonces es un numero negativo por lo tanto vamos  complementarlo, pasarlo a base [10] y poner el signo correspondiente

        > Resto 1
        
            1100 0110 0110 1110 1010 1001 0100 0000
                                                  1 -
            ----------------------------------------
            1100 0110 0110 1110 1010 1001 0011 1111 [2]


        > Aplico NOT bit por bit

            1100 0110 0110 1110 1010 1001 0011 1111
              11 1001 1001 0001 0101 0110 1100 0000 [2]
               

        > Paso a base [10]

           29 28 27 26 25 24 23 22 21 20 19 18 17 16 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
            1  1  1  0  0  1  1  0  0  1  0  0  0  1  0  1  0  1  0  1 1 0 1 1 0 0 0 0 0 0 [2] =

            = 2^6 + 2^7 + 2^9 + 2^10 + 2^12 + 2^14 + 2^16 + 2^20 + 2^23 + 2^24 + 2^27 + 2^28 + 2^29 [10] = 965.826.240 [10]

            El numero es negativo entonces -965.826.240 [10]

        .------------------------------------------------------------------.
        | Configuracion: C66EA940 [16]                                     |
        |                                                                  |
        | Formato: 1100 0110 0110 1110 1010 1001 0100 0000 BPF C/s 32 bits |
        |                                                                  |
        | Valor: -965.826.240 [10]                                         |
        '------------------------------------------------------------------' 
        

    b. 276012 [8]
    
        Del SN8-b sabemos que el formato es 0000 0000 0000 0001 0111 1100 0000 1010 pero esta vez BPF c/s 32 bits

        Al arrancar con 0 es un numero positivo y se sigue los mismos pasos que el otro ejercicio por lo tanto:

        .------------------------------------------------------------------.
        | Configuracion: 276012 [8]                                        |
        |                                                                  |
        | Formato: 0000 0000 0000 0001 0111 1100 0000 1010 BPF c/s 32 bits |
        |                                                                  |
        | Valor: 97.290 [10]                                               |
        '------------------------------------------------------------------' 

    c. F64C6F5B[16]

        Del SN8-c sabemos que el formato es 1100 0110 0110 1110 1010 1001 0100 0000 pero esta vez BPF c/s 32 bits

        El numero arranca con 1, entonces es un numero negativo por lo tanto vamos  complementarlo, pasarlo a base [10] y poner el signo correspondiente

        > Resto 1
        
            1111 0110 0100 1100 0110 1111 0101 1011
                                                  1 -
            ----------------------------------------
            1111 0110 0100 1100 0110 1111 0101 1010 [2]


        > Aplico NOT bit por bit

            1111 0110 0100 1100 0110 1111 0101 1010
                 1001 1011 0011 1001 0000 1010 0101 [2]
               

        > Paso a base [10]

            27 26 25 24 23 22 21 20 19 18 17 16 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
             1  0  0  1  1  0  1  1  0  0  1  1  1  0  0  1  0  0 0 0 1 0 1 0 0 1 0 1 [2] = 1 + 4 + 2^5 + 2^7 + 2^12 + 2^15 + 2^16 + 2^17 + 2^20 + 2^21 + 2^23 + 2^24 + 2^27 [10]

            1 + 4 + 2^5 + 2^7 + 2^12 + 2^15 + 2^16 + 2^17 + 2^20 + 2^21 + 2^23 + 2^24 + 2^27 [10] = 162.762.917

            El numero es negativo entonces -162.762.917 [10]

        .------------------------------------------------------------------.
        | Configuracion: F64C6F5B [16]                                     |
        |                                                                  |
        | Formato: 1111 0110 0100 1100 0110 1111 0101 1011 BPF s/s 32 bits |
        |                                                                  |
        | Valor: -162.762.917[10]                                          |
        '------------------------------------------------------------------' 

SN11

	a) 0,100111[2]

    EL numero ya esta en binario entonces paso a normalizarlo

    0,100111[2] = 1,00111[2] . 10^(-1)[10]

    > Signo: 

      Positivo entonces bit 0
    
    > Exponente:

      -1 en exceso 127 es 126

      126[10] = 1111110[2] (7 bits) <- no lo hago paso a paso

    > Mantisa:

      1,00111[2] con primer bit explicito 00111[2]

    Junto y completo

    0 | 01111110 | 00111000000000000000000

    S     Exp.            Mantisa

    Un cero a la izquierda al exponente para completar (8 bits) y 0 a la izquierda para la mantisa (23 bits)


    > Paso a Hexa:

      0011 | 1111 | 0001 | 1100 | 0000 | 0000 | 0000 | 0000
        3  |   F  |   1  |   C  |   0  |   0  |   0  |   0


    .------------------------------------------------------------------.
    |                                                                  |
    | Formato: 0 01111110 00111000000000000000000 IEEE 754 simple      |
    |                                                                  |
    | Configuracion: 3F1C0000 [16]                                     |
    |                                                                  |
    '------------------------------------------------------------------'

  b) 0,0311[10]

    > Paso a binario

      0,0311 . 2  | 0   
      0,0622 . 2  | 0   
      0,1244 . 2  | 0   
      0,2488 . 2  | 0   
      0,4976 . 2  | 0   
      0,9952 . 2  | 1    -> Primer bit explicito   
      0,9904 . 2  | 1    1
      0,9808 . 2  | 1    2
      0,9616 . 2  | 1    3
      0,9232 . 2  | 1    4
      0,8464 . 2  | 1    5
      0,6928 . 2  | 1    6
      0,3856 . 2  | 0    7
      0,7712 . 2  | 1    8
      0,5424 . 2  | 1    9
      0,0848 . 2  | 0    10
      0,1696 . 2  | 0    11
      0,3392 . 2  | 0    12
      0,6784 . 2  | 1    13
      0,3568 . 2  | 0    14
      0,7136 . 2  | 1    15
      0,4272 . 2  | 0    16
      0,8544 . 2  | 1    17
      0,7088 . 2  | 1    18
      0,4176 . 2  | 0    19
      0,8352 . 2  | 1    20
      0,6704 . 2  | 1    21
      0,3408 . 2  | 0    22
      0,6816 . 2  | 1    23 -> No entra mas que 23 bits asi que paro



      0,0311[10] aprox. 0,00000111111101100010101101101[2]

    > Normalizo

      0,00000111111101100010101101101[2] = 1,11111101100010101101101[2] . 10^(-6)[10]

    > Signo: positivo -> 0

    > Exponente:

     -6 en exceso 17 es 121

     121[10] = 1111001[2] -> 7 bits

    > Mantisa: 

      1,11111101100010101101101[2] = 11111101100010101101101[2]

    > Completo:
    
      0 | 01111001 | 11111101100010101101101

    > Paso a configuracion hexa:

      0011 | 1100 | 1111 | 1110 | 1100 | 0101 | 0110 | 1101
       3   |  C   |  F   |  E   |  C   |  5   |  6   |  D


    .--------------------------------------------------------------.
    |                                                              |
    | Formato: 0 01111001 11111101100010101101101 IEEE 754 simple  |
    |                                                              |
    | Configuracion: 3CFEC56D [16]                                 |
    |                                                              |
    '--------------------------------------------------------------'


  c. 93,F1[16]

    Paso a binario:

     |  9   |  3   |,|  F   |  1   |
     | 1001 | 0011 |,| 1111 | 0001 |


     93,F1[16] = 10010011,11110001[2]

    > Normalizo:

      10010011,11110001[2] = 1,001001111110001[2] . 10^(7)[10] 

    > Signo: Positivo -> 0

    > Exponente:

     7[10] en exceso 127 -> 134[10]

     134/2 = 67 | 0
      67/2 = 33 | 1
      33/2 = 16 | 1
      16/2 =  8 | 0
       8/2 =  4 | 0
       4/2 =  2 | 0
       2/2 =  2 | 0
                | 1

      134[10] = 10000110[2] -> 8 bits esta completo

    > Mantisa:

      1,001001111110001[2] -> 001001111110001  15 bits, se completa con 0 a la derecha


    Juntando:

      0 10000110 00100111111000100000000 [2] IEEE 754 simple

    > Pasando a configuracion hexa

     | 0100 | 0011 | 0001 | 0011 | 1111 | 0001 | 0000 | 0000 |
     |   4  |   3  |   1  |   3  |   F  |   1  |   0  |   0  |


    .--------------------------------------------------------------.
    |                                                              |
    | Formato: 0 10000110 00100111111000100000000 IEEE 754 simple  |
    |                                                              |
    | Configuracion: 4313F100 [16]                                 |
    |                                                              |
    '--------------------------------------------------------------'


SN12.- Dados los siguientes números, almacenarlos en binario de punto flotante IEEE 754 de precisión doble e indicar cual es su configuración
      decimal y octal.

  a. 111001,001 [2]


  b. 29FE3,F [16]



  c. -2145,85 x 10^2  [10]

SN13.- Indique los pasos que serán necesarios para obtener la configuración binaria del siguiente número, expresado como binario de punto flotante
IEEE 754 de precisión simple. Indicar que hacer y para que.

9B9A,36C8 x 11 10 [14]

SN14.- Indicar los números máximos y mínimos, positivos y negativos, que pueden ser almacenados en el formato flotante IEEE 754 de precisión simple.


SN15.- Dadas las siguientes configuraciones de binarios de punto flotante IEEE 754 de precisión simple, indicar el número almacenado en base 10.

  a. 86157840 [16] b. 321200235 [8] c. 00000000 [16]

  d. 2147483648 [10] e. 86E4785A [16] f. 17740000000 [8]


SN16.- Expresar en base 2, los máximos y mínimos números almacenables en 32 bits de un binario de punto flotante cuyo formato es distinto al
      conocido ya que los primeros 26 bits representan la mantisa, los 5 restantes el exponente en exceso y el último bit el signo.

      0                                                       25                        30   31
      |-------------------------------------------------------|-------------------------|----|
                          (mantisa)                                (característica)     (signo)

Nota: La mantisa debe estar normalizada en binario. Tener en cuenta el “1” implícito al igual que el formato IEEE 754 tradicional.


SN17.- Dadas las siguientes configuraciones de empaquetados indicar que número se encuentra almacenado en base 10.
  a. 14302475[8] b. 2076 [10] c. 59 [10]

SN18.- Realizar las siguientes operaciones en la base indicada y expresar el resultado como la configuración hexadecimal y decimal de un 
        empaquetado (si es posible).
  a. A327 + FEC6[16] b. 10210 - 3333 [4]

SN19.- Dados los siguientes números que representan configuraciones de caracteres ASCII indicar cual es el contenido de la cadena de
      caracteres.

a. 110236461011004652523442117 [8]
b. 001010110011010100111000001110010011000000100100[2] 

SN20.- Dados los siguientes números que representan configuraciones de caracteres EBCDIC indicar cual es el contenido de la cadena de
        caracteres.
a. 302130121000103130011232133230021131[4]
b. D6D9C7C1F7F54BF0F3[16]

SN21.- Representar los siguientes caracteres en formato EBCDIC dando su configuración octal.
  a. 458712 b. G67*fas3 c. ADIOS.

SN22.- Idem ejercicio 23 pero para ASCII.
  a. 458712 b. G67*fas3 c. ADIOS.

SN23.- La siguiente es la configuración en base 4 de una cadena de caracteres expresada en código EBCDIC. Interpretar los caracteres de dicha
       cadena como la configuración en base 10 de un empaquetado e indicar el número que se encuentra almacenado en base 10.

  3301331233033320331133003313

SN24.- Dada 2013868923 configuración decimal de un número B empaquetado de 4 bytes expresar:

  a. La configuración hexadecimal del número B representado como binario de punto fijo con signo de 32 bits.
  b. La configuración octal del número B representado como binario de punto flotante IEEE 754 de precisión simple.

SN25.- Indique que formato utilizaría para almacenar el número 2810,33[10] (Justificar)
  a. Empaquetado de 4 bytes.
  b. Binario de punto fijo con signo de 32 bits.
  c. Flotante IEEE 754 de precisión simple.

SN26.- Se tiene un nuevo formato (no IEEE 754) para almacenar binarios de punto flotante de la siguiente manera:

  8 bits para el exponente, almacenado como binario de punto fijo con signo (no hay casos de exponentes especiales).
  24 bits para la mantisa que debe ser almacenada normalizada en base 2, con bit 1 implícito delante de la coma.

  Se pide:
  a. Determinar los máximos y mínimos números almacenables en el formato indicado.
  b. Indicar ventajas y desventajas de este formato, comparando con el formato de precisión simple IEEE 754.

SN27.- Dada la siguiente cadena de memoria de una arquitectura IBM que se encuentran entre las direcciones 35[16] y 3A[16] inclusive:
      330100021001022120221303

  a. Indicar en que base se encuentra.
  b. Obtener el empaquetado de longitud máxima.
  c. Almacenar el número hallado en b como binario de punto flotante IEEE 754 de precisión simple.