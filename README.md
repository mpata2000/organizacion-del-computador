# **Organizacion del Computador**

- [**Organizacion del Computador**](#organizacion-del-computador)
  - [**Sistemas de Numeracion**](#sistemas-de-numeracion)
    - [**Tabla de comparativa**](#tabla-de-comparativa)
    - [**Operaciones arimeticas en otras bases**](#operaciones-arimeticas-en-otras-bases)
    - [**Teorema Fundamental de la numeracion**](#teorema-fundamental-de-la-numeracion)
    - [**Mecanismos de pasaje de base**](#mecanismos-de-pasaje-de-base)
      - [**Pasaje de base *[x]* a base 10**](#pasaje-de-base-x-a-base-10)
        - [**Enteros de base *[X]* a base 10**](#enteros-de-base-x-a-base-10)
        - [**Con coma de base *[X]* a base 10**](#con-coma-de-base-x-a-base-10)
      - [**Pasaje de base 10 a base [x]**](#pasaje-de-base-10-a-base-x)
        - [**Enteros de base 10 a base [x]**](#enteros-de-base-10-a-base-x)
        - [**Con coma de base 10 a base [x]**](#con-coma-de-base-10-a-base-x)
      - [**Pasaje raiz exacta**](#pasaje-raiz-exacta)
      - [**Pasaje potencia exacta**](#pasaje-potencia-exacta)
      - [**Para Numeros periodicos**](#para-numeros-periodicos)
    - [**Representación de números negativos**](#representación-de-números-negativos)
      - [**Signo y magnitud**](#signo-y-magnitud)
        - [**Ventajas y desventajas de Signo y magnitud**](#ventajas-y-desventajas-de-signo-y-magnitud)
      - [**Complemento a 1**](#complemento-a-1)
        - [**Ventajas y desventajas de Complemento a 1**](#ventajas-y-desventajas-de-complemento-a-1)
      - [**Complemento a 2**](#complemento-a-2)
        - [**Ventajas y desventajas de Complemento a 2**](#ventajas-y-desventajas-de-complemento-a-2)
      - [**Complemento a la Base**](#complemento-a-la-base)
      - [**Exceso a base**](#exceso-a-base)
        - [**Ventajas y desventajas del Exceso a base**](#ventajas-y-desventajas-del-exceso-a-base)
    - [**Formatos de representacion en una computadora**](#formatos-de-representacion-en-una-computadora)
      - [**Formato binario de Punto Fijo sin Signo [BPF s/s]**](#formato-binario-de-punto-fijo-sin-signo-bpf-ss)
        - [**Almacenar en BPF s/s**](#almacenar-en-bpf-ss)
        - [**Expandir BPF s/s**](#expandir-bpf-ss)
        - [**Truncar BPF s/s**](#truncar-bpf-ss)
      - [**Formato Binario de Punto Fijo con Signo [BPF c/s]**](#formato-binario-de-punto-fijo-con-signo-bpf-cs)
        - [**Almacenar en BPF c/s**](#almacenar-en-bpf-cs)
        - [**Recuperar de BPF c/s**](#recuperar-de-bpf-cs)
        - [**Expandir BPF c/s**](#expandir-bpf-cs)
        - [**Truncar BPF c/s**](#truncar-bpf-cs)
      - [**Formato Empaquetado**](#formato-empaquetado)
        - [**Almacenar en Empaquetado**](#almacenar-en-empaquetado)
      - [**Formato Zondeado**](#formato-zondeado)
        - [**Almacenar en Zondeado**](#almacenar-en-zondeado)
      - [**IEEE 754**](#ieee-754)
        - [**Significados Especiales:**](#significados-especiales)
        - [**Operaciones Especiales:**](#operaciones-especiales)
        - [**¿Cómo almacenar un número en el formato?**](#cómo-almacenar-un-número-en-el-formato)
        - [**¿Cómo recupero un número en el formato?**](#cómo-recupero-un-número-en-el-formato)
  - [**U2 Maquina Elemental**](#u2-maquina-elemental)
    - [**CONCEPTOS PRELIMINARES**](#conceptos-preliminares)
    - [**ARQUITECTURA VON NEUMANN**](#arquitectura-von-neumann)
      - [**Partes de la arquitectura Von Neumann**](#partes-de-la-arquitectura-von-neumann)
    - [**Abacus**](#abacus)
      - [**Descripción de Componentes de Abacus**](#descripción-de-componentes-de-abacus)
        - [**UAL**](#ual)
        - [**UC**](#uc)
        - [**Memoria**](#memoria)
      - [**Registro de Instrucción**](#registro-de-instrucción)
      - [**Relaciones Abacus**](#relaciones-abacus)
      - [**Desarrollo de una instrucción**](#desarrollo-de-una-instrucción)
        - [**Fase de Busqueda:**](#fase-de-busqueda)
        - [**Ejecución de una instrucción:**](#ejecución-de-una-instrucción)
          - [**Suma:**](#suma)
          - [**Carga:**](#carga)
          - [**Almacenamiento:**](#almacenamiento)
          - [**Bifurcación:**](#bifurcación)
    - [**Super Abacus**](#super-abacus)
      - [**Registro de Instrucción Super Abacus**](#registro-de-instrucción-super-abacus)
      - [**Fase de búsqueda super abacus**](#fase-de-búsqueda-super-abacus)
      - [**Sumar Registro:**](#sumar-registro)
      - [**Sumar Inmediato:**](#sumar-inmediato)
      - [**Sumar Palabra en memoria (Registro Indirecto):**](#sumar-palabra-en-memoria-registro-indirecto)
      - [**Sumar Palabra en memoria (Desplazamiento):**](#sumar-palabra-en-memoria-desplazamiento)
  - [**U3 - Arquitectura del conjunto de instrucciones**](#u3---arquitectura-del-conjunto-de-instrucciones)
    - [**ISA (Instruction Set Architecture)**](#isa-instruction-set-architecture)
      - [**Repertorio de instrucciones**](#repertorio-de-instrucciones)
        - [**Tipos de operandos**](#tipos-de-operandos)
        - [**Clasificación según la ubicación de los operandos**](#clasificación-según-la-ubicación-de-los-operandos)
          - [**Suma C=A+B**](#suma-cab)
        - [**Clasificación de la ISA según el número de direcciones**](#clasificación-de-la-isa-según-el-número-de-direcciones)
      - [**Formato de instrucciones (Encoding)**](#formato-de-instrucciones-encoding)
      - [**Tipos de datos**](#tipos-de-datos)
      - [**Modos de direccionamiento**](#modos-de-direccionamiento)
      - [**Memoria-ISA**](#memoria-isa)
      - [**Control de flujo**](#control-de-flujo)
    - [**Modelo de Capas**](#modelo-de-capas)
    - [**Clasificación de computadoras según su poder de cálculo**](#clasificación-de-computadoras-según-su-poder-de-cálculo)
    - [**Arquitectura Harvard**](#arquitectura-harvard)
  - [**U4 - Lenguaje Ensamblador**](#u4---lenguaje-ensamblador)
    - [**Traducción versus Interpretación**](#traducción-versus-interpretación)
    - [**Ensambladores**](#ensambladores)
      - [**Dos pasadas**](#dos-pasadas)
    - [**Código objeto**](#código-objeto)
      - [**Estructura interna**](#estructura-interna)
    - [**Estructura Linker-Loader**](#estructura-linker-loader)
    - [**Linker**](#linker)
    - [**Loader**](#loader)
    - [**Linking**](#linking)
      - [**Estático (linkage editor)**](#estático-linkage-editor)
        - [**Generación del load module**](#generación-del-load-module)
      - [**Linking dinámico**](#linking-dinámico)
        - [**Load time dynamic linking**](#load-time-dynamic-linking)
        - [**Run time dynamic linking**](#run-time-dynamic-linking)
      - [**Loading**](#loading)
  - [**U5 Componentes de un computador**](#u5-componentes-de-un-computador)
    - [**Memoria**](#memoria-1)
      - [**Jerarquia de Memoria**](#jerarquia-de-memoria)
      - [**Caracteristicas de la memoria**](#caracteristicas-de-la-memoria)
        - [**Locación**](#locación)
        - [**Métodos de acceso de unidades de datos**](#métodos-de-acceso-de-unidades-de-datos)
        - [**Parámetros de performance**](#parámetros-de-performance)
        - [**Tipos físicos**](#tipos-físicos)
        - [**Características físicas**](#características-físicas)
      - [**Memoria RAM**](#memoria-ram)
        - [**Dynamic RAM**](#dynamic-ram)
        - [**Static RAM**](#static-ram)
      - [**Memoria CACHE**](#memoria-cache)
        - [**Cómo funciona la memoria cache**](#cómo-funciona-la-memoria-cache)
        - [**Estructura sistema cache/memoria principal**](#estructura-sistema-cachememoria-principal)
    - [**Administracion de Memoria**](#administracion-de-memoria)
      - [**Sistema Operativo**](#sistema-operativo)
      - [**Uniprogramación**](#uniprogramación)
        - [**Administración de memoria simple**](#administración-de-memoria-simple)
      - [**Multiprogramación**](#multiprogramación)
        - [**Administración de memoria por asignación particionada**](#administración-de-memoria-por-asignación-particionada)
        - [**Administración de memoria por asignación particionada reasignable**](#administración-de-memoria-por-asignación-particionada-reasignable)
        - [**Administración de memoria paginada simple**](#administración-de-memoria-paginada-simple)
        - [**Administración de memoria paginada por demanda(memoria virtual)**](#administración-de-memoria-paginada-por-demandamemoria-virtual)
        - [**Administración de memoria por segmentación**](#administración-de-memoria-por-segmentación)
    - [**Módulo de E/S**](#módulo-de-es)
      - [**Funciones del Modulo de E/S**](#funciones-del-modulo-de-es)
      - [**Técnicas para operaciones de E/S**](#técnicas-para-operaciones-de-es)
      - [**Canales y procesadores de E/S**](#canales-y-procesadores-de-es)
    - [**Interrupciones**](#interrupciones)
      - [**Ciclo de Instruccion**](#ciclo-de-instruccion)
      - [**Muliples interrupciones**](#muliples-interrupciones)
        - [**Deshabilitar interrupciones(secuencia)**](#deshabilitar-interrupcionessecuencia)
        - [**Priorizar interrupciones(anidada)**](#priorizar-interrupcionesanidada)
    - [**Procesador**](#procesador)
      - [**RISC vs CISC**](#risc-vs-cisc)
      - [**Arquitectura de procesadores**](#arquitectura-de-procesadores)
      - [**Técnicas Paralelismo**](#técnicas-paralelismo)
        - [***A nivel instrucción***](#a-nivel-instrucción)
        - [***A nivel procesador***](#a-nivel-procesador)
  - [**U6 Almacenamiento Secundario**](#u6-almacenamiento-secundario)
    - [**Cintas magnéticas**](#cintas-magnéticas)
      - [**Técnicas de grabación**](#técnicas-de-grabación)
        - [**Grabación en paralelo**](#grabación-en-paralelo)
        - [**Grabación en serie**](#grabación-en-serie)
        - [**Grabación helicoidal**](#grabación-helicoidal)
      - [**Modos de operación**](#modos-de-operación)
      - [**Usos y características**](#usos-y-características)
    - [**Discos magnéticos**](#discos-magnéticos)
      - [**Organización de discos magnéticos**](#organización-de-discos-magnéticos)
      - [**Características físicas de los discos**](#características-físicas-de-los-discos)
      - [**Parámetros de performance de los discos**](#parámetros-de-performance-de-los-discos)
      - [**RAID (Redundant Array of Independent Disks)**](#raid-redundant-array-of-independent-disks)
        - [**Nivel 0 (Stripping)**](#nivel-0-stripping)
        - [**Nivel 1 (Espejado)**](#nivel-1-espejado)
        - [**Nivel 2 (Redundancia por código de Hamming)**](#nivel-2-redundancia-por-código-de-hamming)
        - [**Nivel 3 (Paridad por intercalamiento de bits)**](#nivel-3-paridad-por-intercalamiento-de-bits)
        - [**Nivel 4 (Paridad por intercalamiento de bloques)**](#nivel-4-paridad-por-intercalamiento-de-bloques)
        - [**Nivel 5 (Paridad por intercalamiento distribuido de bloques)**](#nivel-5-paridad-por-intercalamiento-distribuido-de-bloques)
        - [**Nivel 6 (Doble paridad por intercalamiento distribuido de bloques)**](#nivel-6-doble-paridad-por-intercalamiento-distribuido-de-bloques)
    - [**Medios Opticos**](#medios-opticos)
      - [**CD (Compact Disc)**](#cd-compact-disc)
        - [**CD-ROM (Compact Disc-Read Only Memory)**](#cd-rom-compact-disc-read-only-memory)
        - [**CD-R (Compact Disc-Recordable)**](#cd-r-compact-disc-recordable)
        - [**CD-RW (Compact Disc-Rewriteable)**](#cd-rw-compact-disc-rewriteable)
      - [**DVD (Digital Versatile Disk)**](#dvd-digital-versatile-disk)
      - [**HD (High Definition)**](#hd-high-definition)
      - [**Sony / Panasonic: Tecnología “Archival Disc”**](#sony--panasonic-tecnología-archival-disc)
    - [**SSD (Solid State Drive)**](#ssd-solid-state-drive)
      - [**Comparación con discos magnéticos**](#comparación-con-discos-magnéticos)
      - [**Tecnologías SSD**](#tecnologías-ssd)

## **Sistemas de Numeracion**

### **Tabla de comparativa**

|Decimal|Binario|Octal|Hexa|
|-------|-------|-----|----|
| 00    | 0000  | 00  | 00 |
| 01    | 0001  | 01  | 01 |
| 02    | 0010  | 02  | 02 |
| 03    | 0011  | 03  | 03 |
| 04    | 0100  | 04  | 04 |
| 05    | 0101  | 05  | 05 |
| 06    | 0110  | 06  | 06 |
| 07    | 0111  | 07  | 07 |
| 08    | 1000  | 10  | 08 |
| 09    | 1001  | 11  | 09 |
| 10    | 1010  | 12  | 0A |
| 11    | 1011  | 13  | 0B |
| 12    | 1100  | 14  | 0C |
| 13    | 1101  | 15  | 0D |
| 14    | 1110  | 16  | 0E |
| 15    | 1111  | 17  | 0F |
| 16    | 10000 | 20  | 10 |

- En esta catedra vamos a representa la base como: `numero[base]`
  - Tambien se puede denotar como $`numero_{base}=()_{base}`$
- En todas la bases el cero y el uno se representan como: 0 y 1
- En todas la bases el 10 es igual a la base
  - 10[2]  =  2[10]
  - 10[8]  =  8[10]
  - 10[10] = 10[10]
  - 10[16] = 16[10]

### **Operaciones arimeticas en otras bases**

### **Teorema Fundamental de la numeracion**

$$`C=XB=\sum ^{+\infty} _{-\infty}`$$

donde:

- **C** es un numero en cualquier base
- **x** es el vector de digitos
- **B** es el vector de pesos
- **b** es la base del sistema de numeracion

![a](./Images/Eg_Teorema_fundamental_de_la_numeracion.png)

### **Mecanismos de pasaje de base**

Suele ser asi:

1. Se quiere hace $`N[b] \rightarrow [p]\ o \ N_b\rightarrow ()_p`$
2. Se pasa el numero de base **b** a base 10: $`N[b] \rightarrow [10]\ o \ N_b\rightarrow ()_{10}`$
3. Se pasa el numero en base 10 a base **p**: $`N[10] \rightarrow [p]\ o \ N_{10}\rightarrow ()_{p}`$

> **Recordar:**
>
> Si se tiene un número que posee parte entera y fraccionaria se realiza el
cambio para cada parte por separado y luego se suman los resultados
obtenidos.

#### **Pasaje de base *[x]* a base 10**

Aplicamos el teorema fundamental de la numeracion adaptado

##### **Enteros de base *[X]* a base 10**

```math
\overset{\tiny3}{A}\overset{\tiny2}{B}\overset{\tiny{1}}{C}\overset{\tiny0}{D}[b] = A.b^3+B.b^2+C.b^1+D.b^1[10]
```

Ejemplo

```math
\overset{\tiny3}{1}\overset{\tiny2}{0}\overset{\tiny{1}}{1}\overset{\tiny0}{1}[2] = 1.2^0+1.2^1+0.2^2+1.2^3[10]=11
```

##### **Con coma de base *[X]* a base 10**

```math
0.\overset{\tiny-1}{A}\overset{\tiny-2}{B}\overset{\tiny{-3}}{C}\overset{\tiny-4}{D}[b] = A.b^{-1}+B.b^{-2}+C.b^{-3}+D.b^{-4}[10]
```

#### **Pasaje de base 10 a base [x]**

##### **Enteros de base 10 a base [x]**

Consiste en realizar divisiones sucesivas hasta que el cociente sea menor que el divisor (base deseada), luego juntar el último cociente obtenido junto con todos los restos de abajo hacia arriba.

**Ejemplo**

34[10] -> [2]

34/2 = 17 resto **0**

17/2 = 08 resto **1**

8/2 = 8 resto **0**

4/2 = 2 resto **0**

2/2 = **1** resto **0**

Hemos llegado al final de las divisiones sucesivas notar que 1 < 2

34[10] = 100010[2]

##### **Con coma de base 10 a base [x]**

Se resuelve mediante multiplicaciones sucesivas tomando de cada resultado la parte entera. Se termina cuando:

- Una multiplicacacion da x.000
- Infinitos deigitos
- Se repite una multiplicacion (Hay ciclos)

**Ejemplo**

0,125[10] -> [2]

0,125 * 2 = **0**,250

0,250 * 2 = **0**,500

0,500 * 2 = **1**,000

0,125[10] -> 0,001[2]

#### **Pasaje raiz exacta**

A[a] -> B[b]

Si la base **a** es raiz exacta (x) de **b**

$`a = \sqrt[X]{b}\ \ \ o\ \ \ a^x=b `$*

Agrupamos de a **X** digitos de la base orgien **a** y denero un digito de la base destino **b**

![](Images/raiz%20exacta.png)

#### **Pasaje potencia exacta**

La base a es potencia exacta (x) de b

$`a=b^x\ \ \ o\ \ \ \sqrt[X]{a}=b `$*

Expando un digito de la base origen y genero z digitos de la base destino

![](Images/Potencia%20exacta.png)

#### **Para Numeros periodicos**

Se usa la expresion periodica

- Numerador: NUmero detras de la coma menos la parte no periodica
- Denominador: El digito mayor de la base por cada digito periodico y un cero por cada digito no periodico

**Ejemplo:**

```math
0,4\widehat{56}`= \frac{456-4}{990}
```

### **Representación de números negativos**

#### **Signo y magnitud**

Para representar un número signado de n-bits usando el método de “signo y
magnitud” consiste en:

1. un bit para representar el signo. Ese bit a menudo es el bit más
significativo
   - 0 para números positivos.
   - 1 para números negativos.
2. los n-1 bits restantes para representar el significando que es la magnitud
del número en valor absoluto.

**Ejemplo pasar un numero a Signo y Magnitud:**

Usando 8 bits queremos representar el número -97[10].

1. Al ser un numero negativo un 1 en el primer bit de signo
2. Realizar la conversión:
   1. |-97[10]| = 97[10]
   2. 97[10] = 1100001[2]
3. Juntar todo
   1. 11100001[2] = -97[10]
   2. 01100001[2] = 97[10]

**Ejemplo sacar un numero de Signo y Magnitud:**

10110101[2], procedemos a:

1. Mirar el primer bit de signo, al ser 1 es negativo
2. Sacar el valor absoluto:
   1. 0110101[2]= 57[10]
3. Juntar todo: 10110101[2] = -57[10]

##### **Ventajas y desventajas de Signo y magnitud**

- **Ventajas**
  - Posee un rango simétrico
- **Desventajas**
  - No permite operar aritméticamente
  - Posee doble representación del cero
    - 00000000[2] = +0[10]
    - 10000000[2] = -0[10]

#### **Complemento a 1**

EL complemento a 1 consiste en aplicarle un NOT bit a bit al número

La representación por Complemento a Uno de un número signado de n-bits es:

1. un bit para representar el signo. Ese bit a menudo es el bit más
significativo
   - 0 para números positivos.
   - 1 para números negativos.
2. los n-1 bits restantes para representar el significando que es la magnitud
   - Si es positivo -> Queda igual
   - Si es negativo se hace el complemento (Aplicar not bit a bit)

**Ejemplo**

##### **Ventajas y desventajas de Complemento a 1**

- **Ventajas**
  - Posee un rango simétrico
  - Permite operar aritméticamente y para obtener el resultado correcto

> Al operar se debe sumar el acarreo obtenido al final de la suma/resta realizadas en caso de haberlo obtenido, este acarreo se lo conoce con el nombre de end-around carry.

- **Desventajas**
  - Posee doble representación del cero
    - 00000000[2] = +0[10]
    - 11111111[2] = -0[10]

#### **Complemento a 2**

Permite representar números negativos en el sistema binario y la realización de restas mediante sumas. Tener en cuenta que estos números negativos están representados a través de su complemento.

El complemento a 2 de un número X[2] se obtiene de sumar 1 al número negado bit a bit de X[2], en otras palabras -> `Not (X) + 1`

##### **Ventajas y desventajas de Complemento a 2**

- **Ventajas**
  - No posee doble representación del cero.
  - Permite operar aritméticamente.
- **Desventajas**
  - Posee un rango asimétrico de números

#### **Complemento a la Base**

TODO

#### **Exceso a base**

El exceso a la base consiste en

1. Tomar el valor real del número a representar
2. Sumarle la base elevada según la cantidad de dígitos menos 1 que se
tienen disponibles

Esto se lo conoce como representación en Exceso a base $`(b^{n-1})`$, puesto que a cada número se le suma el mismo valor y está en exceso por dicho valor.

**Ejemplo pasar un numero a Exceso a base:**

Siendo n = 8 la cantidad de bits disponibles (Exceso de 2^{7-1 = 128[10].), representar el número -97[10] (decimal), la manera de representarlo usando Exceso 2n-1 es la siguiente:

1. Tomar el número -97[10] y sumarle el exceso, en este caso 128[10]
   - -97[10] + 128[10] = 31[10].
2. Convertimos a binario:
   - 31[10] = 00011111[2].

**Ejemplo sacar un numero de  Exceso a base:**

Para el caso inverso, dado un número binario en Exceso 128[10], por ejemplo, 10110101[2], procedemos a:

1. Convertir el número a la base deseada
   - 10110101[2] = 181[10];
2. Pero el valor obtenido está en exceso a 128, entonces debemos quitarle dicho exceso, restando 128:
   - 181[10] - 128[10] = 53[10]

##### **Ventajas y desventajas del Exceso a base**

- **Ventajas**
  - No hay empaquetación del número. Esto significa que no hay que recordar que partes del número son signo y valor.
  - Permite operar aritméticamente, teniendo en cuenta que cada operación lleva asociado su exceso y debemos restarlo al resultado final para obtener la correcta representación.
- **Desventajas**
  - Posee un rango asimétrico de números
  - Requiere de operaciones aritméticas intermedias para su obtención.

### **Formatos de representacion en una computadora**

**Expandir formato:**
> Nos referimos a completar su representación computacional sin alterar el número representado en el mismo.

**Truncar formato:**
> Nos referimos a descartar dígitos de su representación sin alterar el número representado en el mismo.

**Pasajes:**

```mermaid
  flowchart TD

  id1[Nro. Valor Absoluto]
  id2[Formato]
  id3[Configuracion]

  id1 -- Representar/Almacenar --> id2
  id2 -- Pasaje de Base --> id3
  id2 -- Interpretar --> id1
  id3 -- Pasaje de Base --> id2

```

**Ejemplo:**

- **Nro. Valor Absoluto:** 13[10]
- **Formato:** `0000000000001101`[2] BPF s/s 16 bits
- **Configuracion:** `0 0 0 D`[16] BPF s/s 16 bits

#### **Formato binario de Punto Fijo sin Signo [BPF s/s]**

- **Tipo de dato:** Numeros enteros, sin signo
- **Capacidad:** n bits(8 bits,16 bits,32 bits,etc...)
- **Rango de valores:** $`0...2^n-1[10]`$

##### **Almacenar en BPF s/s**

 1. Pasar el Numero a base 2
 2. Completar con ceros a izquierda la capacidad(cantidad de bits) del formato

**Ejemplo:**

Almacenar 27[10] en BPF s/s de 8 bits

1. 27[10] = 11011[2]
2. `0000`1101[2]

##### **Expandir BPF s/s**

Se completa con ceros a izquierda.

00011011[2] 8 bits -> 0000000000110011[2] 16 bits

##### **Truncar BPF s/s**

Si es posible se sacan ceros a izquierda.

00001101[2] 8 bits -> 1101[2] 4 bits
00010111[2] 8 bits -> No es posible truncarlo a 4 bits

#### **Formato Binario de Punto Fijo con Signo [BPF c/s]**

- **Base**: 2
- **Tipo de dato:** Numeros enteros, con signo
  - El primer bit es de signo
- **Rango de valores:** $`-2^{n-1}...2^{n-1}-1[10]`$

##### **Almacenar en BPF c/s**

1. Pasar el número a base 2.
2. Completar con ceros a izquierda.
3. Si es un número negativo complementar usando método [Complemento a 2](#complemento-a-2).

##### **Recuperar de BPF c/s**

1. Si el bit de signo (primero de izquierda) es 1, eso implica que el número es negativo, por lo tanto, debemos complementarlo antes, sino dejamos el número tal cual está
2. Quitar los 0 a izquierda.
3. Pasar de base 2 a base 10
4. Colocar el signo que corresponde.

##### **Expandir BPF c/s**

Se completa con el bit de signo a la izquierda.

00011011 BPF c/s 8 bits -> 0000000000110011 BPF c/s  16 bits
10110101 BPF c/s 8 bits -> 1111111110110101 BPF c/s 16 bits

##### **Truncar BPF c/s**

Se extraen bits a izquierda siempre y cuando no se
esté alterando el bit de signo del número.

00000011 BPF c/s 8 bits -> 0011 BPF c/s 4 bits
00001011 BPF c/s 8 bits -> 1011 BPF c/s 4 bits No se puede ya que representan números diferentes

#### **Formato Empaquetado**

- **Base**: 16
- **Tipo de dato:** Enteros positivos y negativos.
- **Rango de valores:** $`-10^{2n-1}+1...10^{2n-1}-1[10]`$

> Un byte se divide en dos conjuntos de 4 bits a cada grupo se lo denominado NIBBLE, en otras palabras, elNIBBLE es el conjunto de cuatro dígitos binarios.

##### **Almacenar en Empaquetado**

1. Pasar el número a base 10.
2. Colocar cada dígito decimal en un nibble, dejando el último nibble ya que en el mismo se almacena el signo.
3. Colocar en el último nibble el signo según:
   - **C,A,F,E** -> Positivo
   - **B,D** -> Negativo

**Ejemplo:**

-127[10] Empaquetado de 3 bytes(6 nibbles a completar)

|0|0|1|2|7|D|

> Se completa con 0 hasta alcanzar lo biytes usados

**Para recuperar el numero seguir los pasos en orden inverso.**

#### **Formato Zondeado**

- **Base**: 16
- **Tipo de dato:** Enteros positivos y negativos.
- **Rango de valores:** $`-10^{n}+1...10^{n}-1[10]`$

##### **Almacenar en Zondeado**

1. Pasar el número a base 10.
2. Colocar cada uno de los dígitos decimales en un Nibble derecho.
3. Completar todos los Nibbles de izquierdo con F salvo el último
4. Colocar en el ante último nibble el signo según:
   - **C,A,F,E** -> Positivo
   - **B,D** -> Negativo

**Ejemplo:**

-127[10] Empaquetado de 4 bytes(8 nibbles a completar)

|F|0|F|1|F|2|B|7|

> se rellena con F0 hasta alcanzar la cantidad de bytes usados

#### **IEEE 754**

![IEEE 754](Images/ieee.png)

##### **Significados Especiales:**

![IEEE 754](Images/ieee-significado.png)

**Cero:** Puesto que el significando se supone almacenado en forma normalizada, no es posible representar el cero (se supone siempre precedido de un 1). Por esta razón se convino que el cero se representaría con valores 0 en el exponente y en el significando. 

**Infinitos:** Se ha convenido que cuando todos los bits del exponente están a 1 y todos los del significando a 0, el valor es +/- infinito (según el valor S). Esta distinción ha permitido al Estándar definir procedimientos para continuar las operaciones después que se ha alcanzado uno de estos valores (después de un overflow).

**Valores no-normalizados** (denominados también "subnormales"). En estos casos no se asume que haya que añadir un 1 al significado para obtener su valor. Se identifican porque todos los bits del exponente son 0 pero el significado presenta un valor distinto de cero (en caso contrario se trataría de un cero).

**Valores no-numéricos:** Denominados NaN ("Not-a-number"). Se identifican por un exponente con todos sus valores a 1, y un significando distinto de cero. Existen dos tipos QNaN ("Quiet NaN") y SNaN ("Signalling NaN"), que se distinguen dependiendo del valor 0/1 del bit más significativo del significando, es decir si ese bit está seteado o no. QNaN tiene el primer bit a 1, y significa "Indeterminado", SNaN tiene el primer bit a 0 y significa "Operación no-válida".

##### **Operaciones Especiales:**

![](Images/operaciones-ieee.png)

##### **¿Cómo almacenar un número en el formato?**

**A = -6,125[10].**

1. El bit 31 tomará el valor del signo del número. (-6,125  1)
2. Pasar a binario la mantisa decimal.
   - 6 = 110[2]
   - 0,125 = 0,001[2]
   - 6,125 = 110,001[2]
3. Normalizar. Correr la coma a derecha o izquierda hasta convertir el número binario en un número de la forma 1,........El número de desplazamientos va a dar valor al exponente de forma que:
   - Desplazamiento a la derecha -> Exponente negativo
   - Desplazamiento a la izquierda -> Exponente positivo
   - 6,12510=110,001[2] -> 1,10001 -> Exponente = 2
   - 2 expresado en Exceso 127 es 129 = 10000001[2]
4. Mantisa representada con bit implícito: 1,10001 -> 10001 (el bit 1 de la parte entera no se representa)
5. El número final es 1 10000001 10001000000000000000000[2] (Se agregan a la derecha los “0” necesarios para completar los 23 bits de la mantisa)
6. Pasado a hexadecimal 1 100 0000 1 100 0100 0000 0000 0000 0000[2] = C0C40000[16]

##### **¿Cómo recupero un número en el formato?**

1. Convertir a binario el número hexadecimal
C0C40000[16] = 1100 0000 1100 0100 0000 0000 0000 0000[2]
2. Identificar los campos de la configuración binaria 1 10000001 10001000000000000000000[2]
   - Signo de la mantisa
   - Exponente representado en exceso 127
   - Mantisa normalizada con bit implícito
3. Convertir cada uno de los campos a decimal
   - 1  Mantisa negativa
   - 10000001[2] = 129[10] -> +2 es el exceso
   - 10001000000000000000000[2]
4. Corremos la coma binaria según el exponente: 1,10001000000000000000000[2] -> debemos desplazar la coma a la derecha en 2 posiciones, quedando 110,001000000000000000000[2]
5. El numero final es la combinación de todos los valores de los campos
   - -6,125[10]

## **U2 Maquina Elemental**

### **CONCEPTOS PRELIMINARES**

- **Registro:** los registros son considerados los bloques más importantes de un computador. Una definición general los identifica como una “memoria muy rápida” que permite almacenar una cierta cantidad de bits (información).
- **Compuerta:** las compuertas constituyen la base del hardware sobre la cual se construyen los computadores digitales. Son circuitos electrónicos biestables unidireccionales, es decir, permiten el pasaje de información en un sólo sentido y admiten únicamente dos estados (abierto / cerrado, 1 / 0).
- **Bus**: sistema digital que transporta datos entre las distintas partes
  - **Bus de Datos:** mueve la información por los componentes de hardware internos y externos del sistema tanto de entrada como de salida (teclado, Mouse etc.)
  - **Bus de Direcciones:** ubica los datos en la memoria teniendo relación directa con los procesos de la CPU.
  - **Bus de Control:** marca el estado de una instrucción que fue dada a la PC.

![](Images/Maquina-Elemental-conceptos-basicos.png)

### **ARQUITECTURA VON NEUMANN**

Es del año 1945. Promueve la mecanización del tratamiento digital de la información. Tiene dos conceptos fundamentales:

- **Principio del programa almacenado:** el computador debe tener el programa almacenado en su propia memoria. El uso de la memoria es para el almacenamiento de datos y las instrucciones del programa.
- **Principio de ruptura de secuencia:** las operaciones de decisión lógica deben ser automáticas, dotando a la máquina de una instrucción llamada “salto condicional”.

![](Images/esquema-von-neumann.png)

#### **Partes de la arquitectura Von Neumann**

- **Memoria:** se encuentra dividida en celdas o posiciones de memoria cuyo contenido es variable y son identificadas por un número fijo llamado dirección de memoria. La capacidad total de una memoria está dada por la cantidad de celdas disponibles, en ella se almacenan dos clases de información: las instrucciones del programa que se deberá ejecutar y los datos (comúnmente llamados operandos) con los cuales deberá trabajar el programa.
- **Unidad aritmético-lógica (UAL):** es la unidad encargada de realizar las operaciones elementales de tipo aritmético (sumas y restas) y de tipo lógico (generalmente comparaciones)
- **Unidad de control (UC):** desde ella se controlan y gobiernan todas las operaciones (búsqueda, decodificación y ejecución de instrucciones). Es la encargada de extraer de la memoria central la nueva instrucción a ejecutar, analizarla y extraer también los operandos implicados. A su vez, desencadena el tratamiento de los datos en la UAL y, de ser necesario, los almacena en la memoria central.
- **Dispositivo de E/S:** gestiona la transferencia de información entre los periféricos y la memoria central.
- **Bus de datos:** es un sistema digital que proporciona un medio de transporte de datos entre las distintas partes, no almacena información sólo la transmite

### **Abacus**

![](Images/abacus.png)

Maneja una aritmética de Binario Punto Fijo con signo de 16 bits.

**Registros:**

- **AC:** acumulador
- **RI:** registro de instrucción
- **SI:** secuenciador de instrucciones
- **RPI:** registro de próxima instrucción
- **RI:** registro de instrucción
- **RDM:** registro de direcciones de memoria
- **RM:** registro de memoria

#### **Descripción de Componentes de Abacus**

##### **UAL**

Abacus es una máquina de una sóla dirección, su unidad aritmético-lógica posee un registro particular llamado acumulador (**AC**) que sirve tanto para albergar el primer operando como para albergar el resultado. Esta característica permite instrucciones de una sóla dirección: la del segundo operando.

**Operaciones:**

- CARGA (cargar un contenido en el AC)
- ALMACENAMIENTO (enviar datos a memoria por el bus)
- SUMA (sumar un dato a lo que haya en el AC)
- LOGICAS (OR, AND, XOR)

> EN Abacus todas las operaciones se realizan “contra” el acumulador y el resultado siempre queda almacenado en él

##### **UC**

La UC contiene tres registros:

- **RPI:** contiene la dirección de la próxima instrucción a ejecutar. A medida que se van ejecutando las instrucciones este registro aumenta su contenido en una unidad excepto para las instrucciones de ruptura de secuencia.
  - se comunica con la memoria y con el RI a través del Bus de Direcciones
- **RI:** contiene la instrucción extraída de la memoria.
  - se comunica con la memoria mediante el Bus de Datos.
- **SI:** administra la apertura y cierre de compuertas.

##### **Memoria**

La memoria tiene dos registros:

- **RDM:** contiene la dirección de la celda de memoria.
- **RM:** contiene el dato de la celda de memoria.

#### **Registro de Instrucción**

| CO (Código de Operación) | OP (Operando)|
|--------------------------|--------------|

#### **Relaciones Abacus**

- Tamaño RPI = tamaño RDM = tamaño OP = cantidad de celdas direccionables
- Tamaño AC = tamaño ir = tamaño RM = longitud de instrucción = longitud de celda
- Maneja una aritmética de Binario Punto Fijo con signo de 16 bits.

#### **Desarrollo de una instrucción**

##### **Fase de Busqueda:**

Consiste en localizar la instrucción que se va a ejecutar; esta fase es común a todos los tipos de instrucciones y, a su vez, la secuencia de acciones que se lleva a cabo es idéntica a la búsqueda de operandos. También se actualiza en forma secuencial la dirección de la siguiente instrucción a ejecutar.

```math
  RDM \longleftarrow (RPI)      \\
  RM  \longleftarrow ((RDM) )   \\
  RI  \longleftarrow RM         \\
  RPI \longleftarrow (RPI) + 1
```

##### **Ejecución de una instrucción:**

Implica el movimiento de datos. Los pasos se realizan en forma secuencial; la UCP sigue las señales dadas por el reloj del sistema. Este paso es distinto para cada operación

###### **Suma:**

se debe sumar el contenido del RM al contenido del acumulador

```math
RDM \longleftarrow (Op)       \\
RM  \longleftarrow ((RDM))    \\
AC  \longleftarrow AC + ( RM )
```

###### **Carga:**

se debe almacenar en el acumulador un dato contenido en memoria

```math
RDM \longleftarrow (Op)      \\
RM  \longleftarrow (( RDM )) \\
AC  \longleftarrow ( RM )
```

###### **Almacenamiento:**

se debe “guardar” en memoria el contenido del acumulador

```math
RDM   \longleftarrow ( Op ) \\
RM    \longleftarrow ( AC ) \\
(RDM) \longleftarrow ( RM ) \\
```

###### **Bifurcación:**

se debe “saltar” a la dirección indicada en la instrucción. La dirección de bifurcación debe ser transferida al RPI (para buscar la próxima instrucción a ejecutar).

```math
RPI \longleftarrow ( Op )
```

### **Super Abacus**

Superabacus es una maquina de tercera generación ya que posee un conjunto de registros banalizados, es decir, utilizables tanto como registros aritméticos o, según las condiciones de direccionamiento, como registro base o como registro de índice para cálculo de direcciones. Todos los cálculos se realizan en un solo sumador que actúa a la vez de unidad aritmético-lógica y de unidad de calculo de direcciones.

![](Images/Superabacus.png)

Como características principales podemos enumerar las siguientes:

1. Posee un conjunto de registros generales éstos pueden contener datos o direcciones.
2. No tiene RPI se asigna esa función a R0 (registro cero), cuyo incremento se consigue por transferencia vía el sumador.
3. Es una máquina de dos direcciones (1o y 2o operando).
4. La UAL se utiliza tanto para calcular direcciones como para operar con los datos.

#### **Registro de Instrucción Super Abacus**

![](Images/registro_instruccion_superabacus.png)

#### **Fase de búsqueda super abacus**



#### **Sumar Registro:**

Se suma el contenido de ambos registros y el resultado se almacena en aquel que se indica en el primer operando.

![](Images/sumar-registros.png)

#### **Sumar Inmediato:**

Se suma al registro indicado en el 1o operando el dato inmediato almacenado en la instrucción. El resultado se almacena en el registro.

![](Images/suma-registro-inmediato.png)

#### **Sumar Palabra en memoria (Registro Indirecto):**

Se suma al registro indicado en el primer operando el contenido de la celda de memoria cuya dirección está dada por el contenido del registro indicado en el segundo operando. El resultado queda almacenado en el registro del primer operando:

![](Images/suma-registro-indirecto.png)

```math
RDM \longleftarrow ( R3 )  \\
RM \longleftarrow ( ( RDM ) )  \\
AC \longleftarrow ( R5 ) + ( RM )  \\
R5 \longleftarrow ( AC )  \\
```

#### **Sumar Palabra en memoria (Desplazamiento):** 

Se suma al registro indicado en el primer operando el contenido de la celda de memoria cuya dirección está dada por el contenido del registro indicado en el segundo operando más el Offset. El resultado queda almacenado en el registro del primer operando:

![](Images/suma-offset.png)

## **U3 - Arquitectura del conjunto de instrucciones**

“Son las características computacionales visibles al programador, es decir, los atributos que tienen impacto directo en la ejecución lógica de un programa”

### **ISA (Instruction Set Architecture)**

#### **Repertorio de instrucciones**

- ¿Qué es una instrucción de máquina?
  - Opcode + Operandos (0 a n)
- Categorías
  - Aritméticas y lógicas
  - Movimiento de datos
  - Entrada / Salida
  - Control de flujo

##### **Tipos de operandos**

- Registro
- Memoria
- Inmediato

##### **Clasificación según la ubicación de los operandos**

- Stack
- Acumulador 
- Registro-Memoria 
- Registro-Registro (Load/Store) 
- Memoria-Memoria

![](Images/repertorio-de-instrucciones.png)

###### **Suma C=A+B**

![](Images/repertorio-de-instucciones-2.png)

##### **Clasificación de la ISA según el número de direcciones**

- 0 direcciones (Stack)
- 1 dirección (Acumulador)
- 2 direcciones (Reg-Mem/Reg-Reg/Mem-Mem)
- 3 direcciones (Reg/Mem)

#### **Formato de instrucciones (Encoding)**

- Definición
  - “Define el despliegue de los bits que componen la instrucción”
- Componentes
  - Opcode
  - 0 a n operandos
  - Modo de direccionamiento de cada operando
  - Flags
- Clasificación
  - Fijo -> ARM
  - Variable -> x86
  - Hibrido -> IBM Mainframe

#### **Tipos de datos**

- Numéricos
  - BPF s/s
  - BPF c/s
  - BPFlotante (IEEE 754 o propietarios)
  - BCD (decimales)
- Caracteres
  - ASCII
  - EBCDIC
  - Unicode
- Datos lógicos
- Direcciones

#### **Modos de direccionamiento**

- Inmediato
- Memoria Directo
- Memoria Indirecto
- Registro
- Registro Indirecto
- Desplazamiento
  - Relativo (Program counter)
  - Registro Base
  - Indexado
- Stack

#### **Memoria-ISA**

- Direccionamiento (Celda)
- Tamaño de palabra (Word size)
- Espacio de direcciones (address space)
- Big vs Little Endian
  - Big endian
    - IBM Mainframe
  - Little endian
    - Intel x86

![](Images/endian.png)

#### **Control de flujo**
- Métodos para evaluar condiciones de bifurcación
  - Condition Code (CC)
  - Condition Register
  - Compare and Branch

![](Images/control-de-flujo.png)

### **Modelo de Capas**

![](Images/Modelo-de-Capas.png)

### **Clasificación de computadoras según su poder de cálculo**

- **Supercomputadoras**
  - Extremadamente rápidas
  - Manejan volúmenes de datos enormes
  - Poseen miles de CPU
- **Macrocomputadoras o Mainframes**
  - Muy rápidas
  - Manejan volúmenes de datos muy grandes
  - Poseen cientos de CPU
  - Muy alta disponibilidad
- **Minicomputadoras o servidores middle range**
  - Rápidas
  - Manejan volúmenes de datos grandes
  - Poseen decenas de CPU
- **Microcomputadoras / PC**
  - Uso individual o redes pequeñas a medianas
  - Manejan volúmenes de datos no muy grandes
  - Poseen uno o varios CPU
- **Computadoras portátiles / notebooks**
  - Uso individual portátil
  - Manejan volúmenes de datos no muy grandes
  - Poseen uno o varios CPU
- **Computadoras de mano**
  - Uso individual portátil acotado
  - Manejan volúmenes de datos pequeños
  - Poseen uno o varios CPU

### **Arquitectura Harvard**

![](Images/Arquitectura_Harvard.png)

- Las instrucciones y los datos se almacenan en memorias diferentes
- Hay dos conexiones entre la unidad de control de la CPU y cada sistema de memoria
- Las instrucciones se pueden cargar al mismo tiempo que los datos (instruction fetch y data access en paralelo por distintos buses)
- Se manejan distintos espacios de direcciones para instrucciones y datos lo que dificulta la programación

## **U4 - Lenguaje Ensamblador**

- Lenguaje de máquina / código máquina
  - **Definición:** “Representación binaria de un programa de computadora el cual es leído e interpretado por el computador. Consiste en una secuencia de instrucciones de máquina”
- Lenguaje ensamblador
  - **Definición:** “Representación simbólica del lenguaje de máquina de un procesador específico.”
  - Elementos que lo componen
    - Etiquetas
    - Mnemónicos
    - Operandos
    - Comentarios
  - Tipos de sentencias
    - Instrucciones
    - Directivas (pseudoinstrucciones)
    - Macroinstrucciones



- Transición entre lenguaje de máquina y ensamblador.



### **Traducción versus Interpretación**
- Traductor
  - **Definición:** “Programa que convierte un programa de usuario escrito en un lenguaje (fuente) en otro lenguaje (destino)
  - Clasificación:
    - Compiladores
    - Ensambladores
- Intérprete
  - **Definición:** “Programa que ejecuta directamente un programa de usuario escrito en un lenguaje fuente”

### **Ensambladores**

- **Definición:** “Programa que traduce un programa escrito en lenguaje ensamblador y produce código objeto como salida”
- Traducción 1 a 1 a lenguaje máquina
- Hay dos tipos:
  - Dos pasadas
  - Una pasada


#### **Dos pasadas**
- **Primera**
  - Definición de etiquetas -> Tabla de símbolos
  - LC: location counter (empieza en 0 con el 1er byte del código objeto ensamblado)
  - Se examina cada sentencia de lenguaje ensamblador
  - Determina la longitud de la instrucción de máquina (reconoce opcode + modo de direccionamiento + operandos) para actualizar el LC
  - Revisa directivas al ensamblador.
  - Por cada etiqueta encontrada se fija si está en la tabla de símbolos. Si no lo está la agrega (si es la definición, registra el LC como tal, sino lo registra como referenciando a la etiqueta)
- **Segunda (Traducción)**
  - Traduce el mnemónico en el opcode binario correspondiente
  - Usa el opcode para determinar el formato de la instrucción y la posición y tamaño de cada uno de los campos de la instrucción
  - Traduce cada nombre de operando en el registro o código de memoria apropiado
  - Traduce cada valor inmediato en un string binario en la instrucción
  - Traduce las referencias a etiquetas en el valor apropiado de LC usando la tabla de símbolos
  - Setear otros bits necesarios en la codificación de la instrucción.

### **Código objeto**

**Definición:** “Es la representación en lenguaje de máquina del código fuente programado. Es creado por un compilador o ensamblador y es luego transformado en código ejecutable por el linkeditor”

#### **Estructura interna**

- Identificación: nombre del módulo, longitudes de las partes del módulo
- Tabla de punto de entrada: lista de símbolos que pueden ser referenciados desde otros módulos
- Tabla de referencias externas: lista de símbolos usados en el módulo pero definidos fuera de él y sus referencias en el código
- Código ensamblado y constantes
- Diccionario de reubicabilidad: lista de direcciones a ser reubicadas
- Fin de módulo

![](Images/Codigo-Objeto.png)

### **Estructura Linker-Loader**

![](Images/esctructura.png)

### **Linker**

Definición: “Programa utilitario que combina uno o más archivos con código objeto en un único archivo que contiene código ejecutable o cargable”

### **Loader**

Definición: “Rutina de programa que copia un ejecutable a memoria principal para ser ejecutado”

### **Linking**

- Estático (linkage editor)
- Dinámico
  - Load time dynamic linking 
  - Run time dynamic linking

#### **Estático (linkage editor)**

Cada módulo objeto compilado o ensamblado es creado con referencias relativas al inicio del módulo

Se combinan todos los módulos objeto en un único load module reubicable con todas las referencias relativas al load module

##### **Generación del load module**

1. Construye tabla de todos los módulos objeto y sus longitudes
2. Asigna dirección base a cada módulo en base a esa tabla
3. Busca todas las instrucciones que referencian a memoria y les suma una constante de reubicación igual a la dirección de inicio de su módulo objeto
4. Busca todas las instrucciones que referencian a otros procedimientos e inserta su dirección

![](Images/linking-estatico-1.png)

![](Images/linking-estatico-2.png)

#### **Linking dinámico**

Se difiere la linkedición de algún módulohasta luego de la creación del load module

Dos tipos:

1. Load time dynamic linking
2. Run time dynamic linking

##### **Load time dynamic linking**

Se levanta a memoria el load module

Cualquier referencia a un módulo externo hace que el loader busque ese módulo, lo cargue y cambie la referencia a una dirección relativa desde el inicio del load module

**Ventajas Load time dynamic linking**

- Facilita la actualización de versión del módulo externo porque no hay que recompilar
- El sistema operativo puede cargar y compartir una única versión del módulo externo
- Facilita la creación de módulos de linkeo dinámico a losprogramadores (ej. Bibliotecas .so en Unix)


##### **Run time dynamic linking**

Se pospone el linkeo hasta el tiempo de ejecución

Se mantienen las referencias a módulos externos en el programa cargado

Cuando efectivamente se invoca al módulo externo, el sistema operativo lo busca, lo carga y linkea al módulo llamador.

**Ventajas Run time dynamic linking**

- No ocupo memoria hasta que la necesito (ej. Bibliotecas DLL de Windows)


#### **Loading**

**Loading absoluto**

- El compilador/ensamblador genera direcciones absolutas
- Solo se puede cargar en un único espacio de memoria

**Loading reubicable**

- El compilador/ensamblador genera direcciones relativas al LC=0
- El loader debe sumar un valor X a cada referencia a memoria cuando carga el módulo en memoria
- El load module tiene que tener información para saber cuales son las referencia a memoria a modificar (diccionario de reubicación)

**Loading por registro base**

- Arquitecturas que usan registros base para el direccionamiento
- Se asigna un valor para el registro base asociado a la ubicación en la que se cargó el programa en memoria

**Loading dinámico en tiempo de ejecución**

- Se difiere el cálculo de las direcciones absolutas hasta que realmente se vaya a ejecutar
- El load module se carga a memoria con las direcciones relativas
- La dirección se calcula solo al momento de ejecutar realmente la instrucción (con soporte de hardware especial)

## **U5 Componentes de un computador**

### **Memoria**

Parte de la computadora que permite almacenar los datos y los programas. La memoria  está dividida en celdas, la unidad direccionable más pequeña, cada una de las cuales tiene 2  atributos: contenido (valor) y dirección (invariable, identificada con un número)

#### **Jerarquia de Memoria**

- Tres características a tener en cuenta
  - Capacidad
  - Tiempo de acceso
  - Costo

![](Images/jerarquia-memoria.png)

- A medida que se baja de la pirámide:
  - Costo por bit decreciente
  - Capacidad creciente
  - Tiempo de acceso creciente
  - Frecuencia de acceso de la memoria por parte de procesador decreciente

#### **Caracteristicas de la memoria**

##### **Locación**

- Interna
  - Registros
  - Memoria interna para unidad de control
  - Memoria Cache
- Externa
  - Dispositivos de almacenamiento periféricos (discos, cintas, etc.)

##### **Métodos de acceso de unidades de datos**

- **Acceso secuencial**
  - Unidades de datos: registros (records)
  - Acceso lineal en secuencia
  - Se deben pasar y descartar todos los registros intermedios antes de acceder al registro deseado
  - Tiempo de acceso variable
  - Ej. cintas magnéticas
- **Acceso directo**
  - Dirección única para bloques o registros basada en su posición física
  - Tiempo de acceso variable
  - Ej. discos magnéticos
- **Acceso aleatorio**
  - Cada posición direccionable de memoria tiene un mecanismo de direccionamiento cableado físicamente
  - Tiempo de acceso constante, independiente de la secuencia de accesos anteriores
  - Ej. memoria principal y algunas memorias cache
- **Acceso asociativo**
  - Tipo de acceso aleatorio por comparación de patrón de bits
  - La palabra se busca por una porción de su contenido en vez de por su dirección
  - Cada posición de memoria tiene un mecanismo de direccionamiento propio
  - Tiempo de acceso constante, independiente de la secuencia de accesos anteriores o su ubicación
  - Ej. memorias cache

##### **Parámetros de performance**

- Tiempo de acceso (latencia)
  - Memorias de acceso aleatorio: tiempo necesario para hacer una operación de lectura o escritura
  - Memorias sin acceso aleatorio: tiempo necesario para posicionar el mecanismo de lectura/escritura en la posición deseada
- Tiempo de ciclo de memoria
  - Memorias de acceso aleatorio: tiempo de accesomás el tiempo adicional necesario para que una nueva operación pueda comenzar
- Tasa de transferencia
  - Tasa con la cual los datos son transferidos dentro o fuera de la unidad de memoria
- Memorias de acceso aleatorio: 1/Tiempo de ciclo de memoria
- Memorias sin acceso aleatorio:

![](Images/formula-tasa-transferencia.png)

##### **Tipos físicos**

- Memorias semiconductoras (memoria principal y cache)
- Memorias de superficie magnética (discos y cintas)
- Memorias ópticas (medios ópticos)

##### **Características físicas**

- Memorias volátiles: se pierde su contenido ante la falta de energía eléctrica (Ej. algunas memorias semiconductoras)
- Memorias no volátiles: no se necesita de energía eléctrica para mantener su contenido (Ej. memorias de superficie magnéticas y algunas memorias semiconductoras)
- Memorias de solo lectura: (ROM – Read Only Memory) no se puede borrar su contenido (Ej. algunas memorias semiconductoras)

#### **Memoria RAM**

- Dos tipos de Random Access Memory

##### **Dynamic RAM**

- Celdas que almacenan datos como carga en capacitores (bit 0 o 1 si hay presencia o ausencia de carga en el capacitor)
- Requieren refrescar su carga periódicamente para mantener los datos almacenados
- Se suelen usar en memoria principal

##### **Static RAM**

- Los valores binarios se almacenan usando compuertas lógicas flip-flop
- Son más complejas y grandes que las DRAM
- Son más rápidas que las DRAM
- Se suelen usar en memoria cache

#### **Memoria CACHE**

![](Images/memoria-cache.png)

- Memoria semiconductora más rápida (y costosa) que la principal
- Se ubica entre el procesador y la memoria principal
- Permite mejorar la performance general de acceso a memoria principal
- Contiene una copia de porciones de memoria principal

##### **Cómo funciona la memoria cache**
- CPU trata de leer una palabra de la memoria principal
- Se chequea primero si existe en la memoria cache.
  - Si es así se la entrega al CPU
  - Sino se lee un bloque de memoria principal (número fijo de palabras), se incorpora a la cache y la palabra buscada se entrega al CPU
- Por el principio de localidad de referencia es probable que próximas palabras buscadas estén dentro del bloque de memoria subido a la cache


##### **Estructura sistema cache/memoria principal**

- **Memoria principal**
  - 2^n palabras direccionables (dirección única de n-bits para cada una)
  - Bloques fijos de K palabras cada uno (M bloques)
- **Cache**
  - m bloques llamados líneas
  - Cada línea contiene:
    - K palabras
    - Tag (conjunto de bits para indicar qué bloque está almacenado)
    - Bits de control 

![](Images/estructura-memoria.png)

### **Administracion de Memoria**

#### **Sistema Operativo**

 “Software que administra los recursos del computador, provee servicios y controla la ejecución de otros programas”

- Algunos servicios que provee
  - Schedule de procesos
  - Administración de memoria
- Monitor
  - Parte residente del Sistema Operativo

#### **Uniprogramación**

- Un solo proceso de usuario en ejecución a lavez
- La memoria de usuario está completamente disponible para ese único proceso
- Uso del procesador a lo largo del tiempo

![](Images/uniprogramming.png)

##### **Administración de memoria simple**

- **Sistema con uniprogramación**
- Se divide la memoria en dos partes
  - Monitor del S.O.
  - Programa en ejecución en ese momento
- **Ventajas:**
  - Simplicidad
- Desventajas:
  - Desperdicio de memoria
  - Desaprovechamiento de los recursos del computador

![](Images/admin-mem-simple.png)

#### **Multiprogramación**

- Varios procesos de usuario en ejecución a la vez
- Se divide la memoria de usuario entre los procesos en ejecución
- Se comparte el tiempo de procesador entre los procesos en ejecución (timeslice)
- **Condiciones de finalización de los procesos:**
  - Termina el trabajo
  - Se detecta un error y se cancela
  - Requiere una operación de E/S (suspensión)
  - Termina el timeslice (suspensión)

![](Images/multiprogramming.png)

##### **Administración de memoria por asignación particionada**
- Sistema con multiprogramación
- La memoria de usuario se divide en particiones de tamaño fijo:
  - Iguales
  - Distintas
- **Ventajas:**
  - Permite compartir la memoria entre varios procesos
- **Desventajas:**
  - Desperdicio de memoria por fragmentacion interna y externa

##### **Administración de memoria por asignación particionada reasignable**

- Sistema con multiprogramación
- Swapping
- La memoria de usuario se divide en particiones de tamaño variable
- Compactación para eliminar la fragmentación
- Se usa un recurso de hardware para la realocación
- Realocación dinámica en tiempo de ejecución
- **Ventajas:**
  - Permite compartir la memoria entre varios procesos
  - Elimina el desperdicio por fragmentación interna. Con la compactación se elimina además la fragmentación externa
- **Desventajas:**
  - La tarea de compactación es costosa

##### **Administración de memoria paginada simple**

- Sistema con multiprogramación
- Se divide el address space del proceso en partes iguales
- Se divide la memoria principal en partes iguales (frames)
- Hay una tabla de páginas por proceso
- Hay una lista de frames disponibles
- Se cargan a memoria las páginas del proceso en los frames disponibles 
- Las direcciones lógicas se ven como número de página yun offset
- Se traducen las direcciones lógicas en físicas con soporte del hardware
- La paginación es transparente para el programador
- **Ventajas:**
  - Permite compartir la memoria entre varios procesos
  - Permite el uso no contiguo de la memoria
  - Minimiza la fragmentación interna 
  - Elimina la fragmentación externa
- **Desventajas:**
  - Se requiere subir todas las páginas del proceso a memoria
  - Se requieren estructuras de datos adicionales para mantener información de páginas y frames

##### **Administración de memoria paginada por demanda(memoria virtual)**

- Sistema con multiprogramación
- Solo se cargan a memoria principal las páginas necesarias para la ejecución de un proceso
- Cuando se quiere acceder a una posición de memoria de una página no cargada se produce un page fault
- El page fault dispara una interrupción por hardware atendida por el sistema operativo
- El sistema operativo levanta la página solicitada desde memoria secundaria 
- Si no hay frames libres es necesario bajar páginas a memoria secundaria y reemplazarlas
- Algoritmos para reemplazo de páginas 
- Thrashing: el CPU pasa más tiempo reemplazando páginas que ejecutando instrucciones
- **Ventajas:**
  - No es necesario cargar todas las páginas de un proceso a la vez
  - Maximiza el uso de la memoria al permitir cargar más procesos a la vez
  - Un proceso puede ocupar más memoria de la efectivamente instalada en el computador
- **Desventajas:**
  - Mayor complejidad por la necesidad de implementar el reemplazo de páginas

##### **Administración de memoria por segmentación**

- Sistemas con multiprogramación
- Generalmente visible al programador
- La memoria del programa se ve como un conjunto de segmentos (múltiples espacios de direcciones)
- Los segmentos son de tamaño variable y dinámico
- El sistema operativo administra una tabla de segmentos por proceso
- Permite separar datos e instrucciones
- Permite dar privilegios y protección de memoria 
- Las referencias a memoria se forman con un número de segmento y un offset dentro de él. Con ayuda de hardware se hacen las traducciones de las direcciones lógicas a físicas
- Se pueden usar para implementar memoria virtual
- **Ventajas:**
  - Simplifica el manejo de estructuras de datos con crecimiento
  - Permite compartir información entre procesos dentro de un segmento
  - Permite aplicar protección/privilegios sobre un segmento fácilmente
- **Desventajas:**
  - Fragmentación externa en la memoria principal por no poder alojar un segmento
  - Hardware más complejo que memoria paginada para la traducción de direcciones

### **Módulo de E/S**

- **¿Qué hace?**
  - Conecta a los periféricos con la CPU y la memoria a través del bus del sistema o switch central y permite la
comunicación entre ellos
- **¿Por qué existe?**
  - Amplia variedad de periféricos con distintos métodos de operación
  - La tasa de transferencia de los periféricos es generalmente mucho más lenta que la de la memoria y procesador
  - Los periféricos usan distintos formatos de datos y tamaños de palabra
- **¿Para qué sirve?**
  - Oculta detalles de timing, formatos y electro mecánica de los dispositivos periféricos

#### **Funciones del Modulo de E/S**

- Control & Timing
  - Controla flujo de tráfico entre CPU/Memoria y periféricos
- Comunicación con el procesador
  - Decodificación de comandos
  - Datos
  - Información de estado
  - Reconocimiento de direcciones
- Comunicación con el dispositivo
  - Comandos
  - Información de estado
  - Datos
- Buffering de datos
- Detección de errores

#### **Técnicas para operaciones de E/S**

- E/S Programada
- E/S manejada por interrupciones
- Acceso directo a memoria (DMA)
  - Información enviada por el CPU al DMA
    - Operación (READ o WRITE) vía Línea de Control
    - Dirección del dispositivo vía Línea de Dirección
    - Dirección inicial de memoria para READ o WRITE vía Línea de Datos, almacenado en el address register
    - Cantidad de palabras para READ o WRITE, vía Línea de Datos, almacenado en el data count register

#### **Canales y procesadores de E/S**

- Canales
  - Tienen la habilidad de ejecutar instrucciones de E/S
  - La CPU principal no ejecuta instrucciones de E/S
  - Las instrucciones de E/S se almacenan en memoria principal
  - La CPU le indica al canal de E/S que inicie un programa de canal
  - Tipos de canales
    - **Selectores:** Usa un dispositivo a la vez a través de un controlador de E/S
    - **Multiplexores:** Trabaja con múltiples dispositivos a la vez
- Procesadores
  - Agregan a los canales memoria propia en vez de usar la memoria principal

### **Interrupciones**
- ¿Qué son?
  - “Mecanismos por los cuales otros módulos (E/S, memoria, etc.) interrumpen el normal procesamiento del CPU”
- ¿Para qué existen?
  - “Para mejorar la eficiencia de procesamiento de un computador”
- Clases de interrupciones
  - Hardware (asincrónicas)
    - E/S
    - Reloj (timer)
    - Fallas de hardware
  - Software
    - Excepciones de programa
    - Instrucciones privilegiadas

#### **Ciclo de Instruccion**


En cada ciclo de instruccion de programa, antes de pasar a la siguente instruccion escucha si hay una interrupcion, si la hay, se ejecuta la interrupcion y luego se continua con la siguiente instruccion del programa.

Hay un handler por cada tipo de interrupcion, el handler es un programa que se ejecuta cuando se produce una interrupcion.

#### **Muliples interrupciones**

##### **Deshabilitar interrupciones(secuencia)**

Mientras se ejecuta el handler de una interrupcion, se deshabilitan las interrupciones para evitar que se ejecuten otros handlers.

![](Images/Deshabilitar%20Interrupciones.png)

##### **Priorizar interrupciones(anidada)**

Mientras se ejecuta el handler de una interrupcion, se ejecuta el handler de otra interrupcion, si esta es de mayor prioridad que la primera, se ejecuta el handler de la segunda interrupcion y se vuelve a ejecutar el handler de la primera interrupcion.

![](Images/Priorizar%20Instrucciones.png)

### **Procesador**

Historia
- Primero orientado al hardware (hasta los ‘70) --> **CISC**
- Luego orientado al software (a partir de los ‘80) --> **RISC**

#### **RISC vs CISC**

![](Images/RISCvsCISC.png)

#### **Arquitectura de procesadores**
- Ecuación de Performance
  - MIPS rate = Frecuencia del reloj en MHz (f) * Instrucciones por ciclo (IPC)
- Uniprocesadores
  - SISD (Single Instruction Single Data) (Uniprocesadores)
  - Limitación de la velocidad del reloj (calor)
-  Paralelismo
  - A nivel instrucción
    - Pipelining
    - Dual pipelining
    - Superscalar
    - Multithreading
  - A nivel procesador
    - Procesadores paralelos de datos
    - Multiprocesadores
    - Multicomputadores

#### **Técnicas Paralelismo**

##### ***A nivel instrucción***

- **Pipelining**
  - Solapa la ejecución de las instrucciones para reducir el tiempo total de una secuencia de instrucciones
  - Ejecuta una instrucción por ciclo de reloj
  - Control de dependencia entre las instrucciones
  - Ej. Intel 486
- **Dual pipelining**
  - Ejecuta dos instrucciones por ciclo de reloj
  - Ej. Intel Pentium
- **Superscalar**
  - Ejecuta más de una instrucción por ciclo de reloj
  - N-way / N-issue (N entre 3 y 6)
  - Intel Core
- **Multithreading**
  - Busca incrementar el uso del CPU intercambiando la ejecución entre threads (hilos de ejecución) cuando uno está frenado por alguna causa
  - Thread: Contiene un PC, un conjunto de registros y la pila . Comparten un mismo address space. Se los conoce como “lightweight processes”
  - Proceso: Puede tener uno o más threads, contiene un address space y un estado gestionado por el S.O.
  - El cambio de contexto entre threads es “liviano” en comparación con los procesos, que requieren del S.O.
  - Fine-grained multithreading: 
    - se intercambia el uso del procesador entre threads luego de la ejecución de cada instrucción. 
    - Ej. Procesadores Intel IA-32
  - Coarse-grained multithreading: 
    - se intercambia el uso del procesador entre threads solo luego de algún evento significativo, como puede ser un page fault o un “cache miss”.
    - Ej. Intel Itanium 2

![](Images/parlelismo-instrucciones.png)

##### ***A nivel procesador***
- **Procesadores paralelos de datos**
  - Una sola unidad de control
  - Múltiples procesadores
  - Métodos
    - SIMD – Single Instruction Multiple Data
      - Múltiples procesadores ejecutan la misma secuencia de pasos sobre un conjunto diferente de datos
      - Nvidia Fermi GPU
    - Vectoriales
      - Similar a SIMD
      - Registro vectorial: conjunto de registros convencionales que se cargan desde memoria en una sola instrucción.
      - Se opera por pipelining
      - Ej. Intel Core
- **Multiprocesadores**
  - Múltiples CPUs que comparten memoria común
  - MIMD (Multiple Instruction Multiple data)
  - CPUs fuertemente acoplados
  - Diferentes implementaciones
    - Single bus y memoria compartida (centralizada) (UMA – Uniform memory access) (SMP – Symmetric multiprocessor)
      - Intel Core i7
    - CPUs con memoria local y memoria compartida (NUMA – non-uniform memory access)
      - Intel Itanium 2
- **Multicomputadores**
  - Computadores interconectados con memoria local
  - No hay memoria compartida
  - CPUs ligeramente acoplados - Clusters
  - MIMD (Multiple Instruction Multiple data)
  - Intercambio de mensajes
  - Topologías de grillas, árboles o anillos
  - Ej. IBM Blue Gene/P

## **U6 Almacenamiento Secundario**

### **Cintas magnéticas**

- Poliester flexible cubierto de material magnetizable
  - Carretes abiertos
  - Paquetes cerrados (cartuchos)
- Acceso secuencial a la información: si estoy en el registro 1 y quiero llegar al N tengo que “leer” los N-1 del medio
- Si quiero leer un registro anterior tengo que rebobinar y volver a buscar el registro

#### **Técnicas de grabación**

##### **Grabación en paralelo**

- Técnica usada originalmente
- Cabeza de grabación estacionaria
- Se graban pistas en paralelo a lo largo de la cinta
- Al principio eran de 9 pistas (8 bits de datos y 1 bit de paridad para detectar errores)
- Luego fueron 18 (palabra) o 36 (doble palabra) pistas

##### **Grabación en serie**

- Sistema moderno de grabación
- Cabeza de grabación estacionaria
- Se escriben los datos a lo largo de una pista primero hasta llegar al final de la cinta y luego se pasa a otra
- Grabación en “serpentina”
- Pueden grabarse n pistas adyacentes en simultáneo (n entre 2 y 8)

##### **Grabación helicoidal**

- Cabeza de grabación rotatoria
- Símil video casseteras
- Evita problema de movimiento veloz de la cinta de las otras técnicas
- La cinta se mueve en forma lenta mientras que la cabeza rota en forma rápida
- Las pistas pueden estar más cercanas unas a otras

#### **Modos de operación**

- Modo start-stop por bloque
  - Viejo uso de grabación por registro/bloque
  - La cinta se usaba para guardar archivos para procesamiento posterior
  - Se podía actualizar un registro/bloque particular siempre y cuando no cambiara su tamaño
  - Los datos se grababan en bloques físicos
  - Entre los bloques había espacios para sincronización de la unidad
- Modo streaming
  - Uso para backup o archivo de información
  - No se requiere operación de start-stop por bloque
  - No se requiere actualización de bloques particulares dentro de un archivo
  - Se escriben archivos completos como un “stream” de datos contiguo
  - La información de graba físicamente en bloques pero no se pueden localizar o modificar bloques particulares

#### **Usos y características**

- Fue el primer medio de almacenamiento secundario
- Aun es usado para backup y archivo de información (30 años o más de duración) dado su bajo costo por byte y su capacidad de almacenamiento
- Es el medio más lento de la pirámide de jerarquía de memoria
- Marcas físicas en las cintas
  - BOT (Beginning of tape)
  - EOT (End of tape)

### **Discos magnéticos**

- Plato circular construido de un material no magnético, llamado substrato, cubierto por un material magnetizable
- Mecanismos de lectura/escritura magnético
  - **Cabeza de lectura/escritura única:** bobina conductora estática, el disco está girando constantemente debajo de ella
    - Usado en los viejos discos rígidos y floppy disk
    - Escritura: cuando circula electricidad a través de una bobina se produce un campo magnético. Los patrones magnéticos resultantes se graban en la superficie
    - Lectura: un campo magnético que se mueve por una bobina produce corriente eléctrica en ella. Cuando la superficie del disco pasa debajo de la cabeza se genera una corriente de la misma polaridad grabada
  - **Cabeza de lectura diferenciada de la de escritura**
    - Tiene un sensor magneto-resistivo (MR)
    - La resistencia eléctrica del material depende de la dirección de la magnetización del medio que se mueve por debajo
    - Se hace pasar una corriente a través del sensor MR y los cambios de resistencia se detectan como señales de voltaje
    - Provee mayores densidades de grabación y velocidades de operación que el mecanismo anterior

#### **Organización de discos magnéticos**

- **Pistas concéntricas**
  - El ancho de la pista es igual al ancho de la cabeza lectora/grabadora
    - Entre las pistas hay un gap para minimizar errores de desalineamiento de la cabeza e interferencias magnéticas
  - La superficie del disco está subdividida en sectores
    - Hay un gap entre los sectores para evitar errores de sincronización
  - El disco gira a velocidad constante
  - La cabeza lectora/grabadora puede operar a la misma tasa de transferencia
  - Los bits exteriores giran a mayor velocidad que los interiores 
  - Para compensar, los bits exteriores están más espaciados entre sí
  - **Ventaja:** se puede referenciar a cada bloque de información a través de pista/sector
  - **Desventaja:** no se aprovecha el máximo de densidad (bits por pulgada lineal) de la superficie del disco
- **Grabación multizona**
  - La superficie del disco se divide en zonas concéntricas
  - La cantidad de bits por pista dentro de una zona es constante
  - Las zonas exteriores contienen más bits por pulgada que las zonas interiores
  - **Ventaja:** mayor capacidad de almacenamiento
  - **Desventajas:** mayor complejidad en la circuitería para trabajar con tiempos de lectura/escritura diferentes según la zona

#### **Características físicas de los discos**

- Movimiento de la cabeza
  - Fija: había una cabeza lectora/grabadora por pista (muy costosos, no se usa) 
  - Móvil: hay una única cabeza lectora/grabadora por superficie del plato. Se mueve por todas las pistas y está montada en un brazo
- Portabilidad
  - Discos no removibles: disco rígido (se monta en un disk drive
  - Removibles: se puede sacar y poner en la unidad (Ej. floppy disk)
- Lados
  - Un solo lado: solo es usable una cara
  - Dos lados: el recubrimiento magnético está en ambas caras
- Platos
  - Un solo plato
  - Múltiples platos: varios discos en un mismo disk drive
- Mecanismo de la cabeza
  - Contacto: toma contacto con la superficie del disco
  - Espacio fijo: se ubica a una posición fija por encima del disco
  - Espacio aerodinámico (flotante): se ubica flotante por sobre el disco gracias a la presión de aire que genera la rotación del disco

#### **Parámetros de performance de los discos**
- Desempeño del disco: depende del computador, sistema operativo, módulo de E/S y controlador de disco
- Tiempo de seek: tiempo necesario para mover la cabeza lectora/grabadora a la pista deseada
- Tiempo de demora rotacional o latencia: tiempo de espera hasta que el sector deseado pasa por la cabeza lectora/grabadora
- Tiempo de acceso: tiempo necesario para estar en posición para escribir o leer
- Tiempo de transferencia: tiempo necesario para transferir la información al disco

#### **RAID (Redundant Array of Independent Disks)**

- Vectores de discos que operan en forma independiente y en paralelo
- Se puede manejar un pedido de E/S en paralelo si los datos residen en discos separados
- Hay distintas maneras de organizar la información y agregarle confiabilidad a los datos
- RAID es un estandar que consiste en 7 niveles (0 a 6). Pueden implementarse combinaciones de niveles
- Es un conjunto de discos que son vistos por el sistema operativo como una única unidad lógica
- Los datos se distribuyen en los discos del vector en un esquema llamado “striping”
- Se usa capacidad redundante para guardar información de paridad y garantizar recuperación ante fallas

##### **Nivel 0 (Stripping)**

- No incluye redundancia
- Se requieren N discos
- Se distribuyen los datos en el vector de discos en strips 
- **Ventajas:**
  - Simplicidad
  - Performance
- **Desventaja:**
  - Riesgo ante fallos, no hay recuperación posible

![RAID 0](Images/RAID0.png)

##### **Nivel 1 (Espejado)**

- Redundancia por espejado de datos
- Se requieren 2N discos
- **Ventajas:**
  - Un pedido de lectura puede resolverse por cualquiera de los dos discos
  - La escritura se hace en forma independiente en cada disco y no se penaliza
  - Simple recuperación ante fallas
  - Alta disponibilidad de datos
- **Desventajas:**
  - Costo

![RAID 1](Images/RAID1.png)

##### **Nivel 2 (Redundancia por código de Hamming)**

- Strips pequeños (un byte o palabra)
- Se calcula redundancia por código autocorrector (ej. Hamming)
- Se requieren N + m discos
- Se graban bits de paridad en discos separados
- Se leen/escriben todos los discos en paralelo, en forma sincronizada
- No existe uso comercial
- **Ventajas:**
  - Disponibilidad de datos
- **Desventaja:**
  - Costos por método de redundancia

![RAID 2](Images/RAID2.png)

##### **Nivel 3 (Paridad por intercalamiento de bits)**

- Solo se usa un disco de paridad
- Se requieren N+1 discos
- La paridad se calcula mediante un bit a través del conjunto individual de bits
de la misma posición de todos los discos
- Se leen/escriben todos los discos en paralelo, en forma sincronizada
- **Ventajas:**
  - Cálculo sencillo de paridad
  - No hay impacto significativo de performance ante fallas
- **Desventajas:**
  - Controlador complejo

![RAID 3](Images/RAID3.png)

##### **Nivel 4 (Paridad por intercalamiento de bloques)**

- Se accede en forma independiente a cada disco
- Se requieren N+1 discos
- Se puede dar servicio a pedidos de E/S en paralelo
- Se usan strips grandes.
- Los bits de paridad se calculan igual que en RAID 3 y se guarda un strip de paridad
- No hay uso comercial
- **Ventajas:**
  - Altas tasas de lectura
- **Desventaja:**
  - Dos lecturas y dos escrituras en caso de update de datos
  - Cuello de botella por disco de paridad

![RAID 4](Images/RAID4.png)

##### **Nivel 5 (Paridad por intercalamiento distribuido de bloques)**

- Se accede en forma independiente a cada disco
- Se requieren N+1 discos
- Los strips de paridad se distribuyen en todos los discos
- **Ventajas:**
  - Resuelve el cuello de botella del nivel 4
- **Desventajas:**
  - Controlador complejo

![RAID 5](Images/RAID5.png)

##### **Nivel 6 (Doble paridad por intercalamiento distribuido de bloques)**

- Se accede en forma independiente a cada disco
- Se requieren N+2 discos
- Se usan dos algoritmos de control de paridad
- **Ventajas:**
  - Provee disponibilidad de datos extremadamente alta
- **Desventaja:**
  - Controlador complejo
  - Costos por doble paridad

![RAID 6](Images/RAID6.png)

### **Medios Opticos**

#### **CD (Compact Disc)**

- CLV (Constant Linear Velocity)
- Pista única en forma de espiral
- Sectores (2352 bytes)
- Master:
  - Se graba con un laser infrarrojo de alto poder
  - Se marcan huecos sobre un disco de vidrio
  - Se hace un molde y se le inyecta policarbonato que sigue los patrones de los huecos
  - Se pone luego una capa reflectiva de aluminio sobre el sustrato
  - Encima se coloca una capa protectora de laca y una etiqueta
  - Pit: marca física (hueco)
  - Land: espacio de la superficie sin marcas
- Bit en 1: transición entre pit-land o land-pit
- Bit en 0: ausencia de transición en un tiempo determinado =>
CLV

##### **CD-ROM (Compact Disc-Read Only Memory)**

- Encoding EFM (Eight to Fourteen Modulation)
- Modos
  - Modo 1 (Datos): 2048 bytes de datos
  - Modo 2 (Audio): 2336 bytes => 74 minutos
- Filesystem (Modo 1): ISO 9660

##### **CD-R (Compact Disc-Recordable)**

- Se podía escribir solo una vez
- En vez de hacer huecos físicos se usa una capa de tinta que inicialmente es transparente y deja pasar la luz laser
- El laser quema la capa de tinta y crea un punto negro que ya no refleja la luz simulando un pit

##### **CD-RW (Compact Disc-Rewriteable)**

- Se usa un material que puede tener dos estados con reflectividades diferentes
- Para escribir se usa alto poder en el laser para pasar el material de estado cristalino a amorfo
- Para borrar se usa medio poder en el laser y se pasa el material de estado amorfo a cristalino nuevamente
- Para leer se usa bajo poder en el laser

#### **DVD (Digital Versatile Disk)**
- Diseño general igual al de los CDs
- Pensados para la industria de distribución de video
- Mejoras con respecto al CD:
  - Pits más pequeños
  - Espiral más encimada
  - Laser color rojo
- Capacidad mejorada
  - Un solo lado, una sola capa (4,7 GB)
  - Un solo lado, dos capas (8,5 GB)
  - Dos lados, una sola capa (9,4 GB)
  - Doble lado, dos capas (17 GB)
- DVD-R: usa tecnología similar a CD-R para permitir hacer una grabación única
- DVD-RW: usa tecnología similar a CD-RW para poder grabar/borrar varias veces

#### **HD (High Definition)**
- Se crearon dos estándares para poder distribuir video de alta definición
- HD-DVD (High Definition – Digital Versatile Disk)
- Blue-ray Disc (BD)
  - Blue-ray fue la tecnología que predominó
  - Se basa en un laser azul
  - La capacidad es de 25 GB para la versión de una sola capa y 50 GB para la de doble capa
- BD-R: usa tecnología similar a CD-R para permitir hacer una grabación única
- BD-RE: usa tecnología similar a CD-RW para poder grabar/borrar varias veces

#### **Sony / Panasonic: Tecnología “Archival Disc”**
- Primera Generación (2015)
  - 300 GB de capacidad
  - Double-Sided
  - 3 layers por lado
- Segunda Generación
  - 500 GB de capacidad
  - Mismas características que la primera generación más:
    - Alta densidad lineal
    - Tecnología de cancelación de interferencias entre símbolos
- Tercera Generación
  - 1 TB de capacidad
  - Mismas características que la segunda generación más:
    - Tecnología de grabación multinivel

### **SSD (Solid State Drive)**

Dispositivo de almacenamiento secundario hecho con componentes electrónicos de estado sólido (semiconductores)

#### **Comparación con discos magnéticos**
- Ventajas
  - Arranque más rápido
  - Gran velocidad de lectura y escritura
  - Baja latencia de lectura y escritura
  - Menor consumo de energía
  - Menor producción de calor
  - Sin ruido
  - Menor peso y tamaño
  - Mayor resistencia a golpes, caídas y vibraciones
- Desventajas
  - Precio ($/GB)
  - Menos recuperación ante fallos
  - Capacidad
  - Vida útil

#### **Tecnologías SSD**

- PCIe / SATA (interface externa)
- AHCI / NVME (interface de comunicación)
- M.2 / 2.5” SATA / mSATA (form factor)